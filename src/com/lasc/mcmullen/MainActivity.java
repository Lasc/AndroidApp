package com.lasc.mcmullen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void gotoLiveGameRecAcivity(View v)
	{
		Intent intent = new Intent(this, LiveGameRecording.class);
		startActivity(intent);
	}
}
