package com.lasc.mcmullen;

import java.util.ArrayList;
import java.util.Iterator;

import android.app.Fragment;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;

import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import android.widget.ToggleButton;

public class HomeTeamFragmentTab extends Fragment {

	SQLiteOpenHelper dbhelper;
	SQLiteDatabase database;
	View lastContextMenuButton;
	ImageButton player1, player2, player3, player4, player5, player6, player7,
			player8, player9, player10, player11;

	// Store in the pass array the sequence of players that pass the ball
	// between themselves.
	// pass will be added in the PASSes Table for pass completion stats.
	ArrayList<String> pass = new ArrayList<>();


	ToggleButton recPass, goal, possession, corner, redCard, yellowCard, foul,
			committedFoul, out, offside;
	
	TextView player1Text, player1NoText, player2Text, player2NoText,
			player3Text, player3NoText, player4Text, player4NoText,
			player5Text, player5NoText, player6Text, player6NoText,
			player7Text, player7NoText, player8Text, player8NoText,
			player9Text, player9NoText, player10Text, player10NoText,
			player11Text, player11NoText;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.home_tab_layout, container,
				false);

		dbhelper = new HomeTeamDBHelper(getActivity());

		// register players for Context Menu to support Substitutions
		registerForContextMenu(rootView.findViewById(R.id.imageButton1));
		registerForContextMenu(rootView.findViewById(R.id.ImageButton01));
		registerForContextMenu(rootView.findViewById(R.id.ImageButton02));
		registerForContextMenu(rootView.findViewById(R.id.ImageButton03));
		registerForContextMenu(rootView.findViewById(R.id.ImageButton04));
		registerForContextMenu(rootView.findViewById(R.id.ImageButton05));
		registerForContextMenu(rootView.findViewById(R.id.ImageButton06));
		registerForContextMenu(rootView.findViewById(R.id.ImageButton07));
		registerForContextMenu(rootView.findViewById(R.id.ImageButton08));
		registerForContextMenu(rootView.findViewById(R.id.ImageButton09));
		registerForContextMenu(rootView.findViewById(R.id.ImageButton10));

		// Get handlers for all the General Events Toogle Buttons
		goal = (ToggleButton) rootView.findViewById(R.id.toggleButton2);
		possession = (ToggleButton) rootView.findViewById(R.id.toggleButton3);
		corner = (ToggleButton) rootView.findViewById(R.id.toggleButton4);
		redCard = (ToggleButton) rootView.findViewById(R.id.toggleButton5);
		yellowCard = (ToggleButton) rootView.findViewById(R.id.toggleButton6);
		foul = (ToggleButton) rootView.findViewById(R.id.toggleButton7);
		committedFoul = (ToggleButton) rootView
				.findViewById(R.id.toggleButton8);
		out = (ToggleButton) rootView.findViewById(R.id.toggleButton9);
		offside = (ToggleButton) rootView.findViewById(R.id.ToggleButton01);

		recPass = (ToggleButton) rootView.findViewById(R.id.toggleButton1);
		recPass.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (pass.size() == 0)
					pass.add((String) StatsFragmentTab.getTime());

				if (!recPass.isChecked()) {
					if (pass.size() > 1) {
						Iterator<String> it = pass.iterator();
						Integer i = 1;
						ContentValues content = new ContentValues();

						while (it.hasNext()) {
							content.put("ColumnPass" + i.toString(), it.next());
							i++;
						}
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamPasses", null, content);
						database.close();
						pass.clear();
					} else
						pass.clear();
				}

			}
		});

		// ############   PLAYER 1     #######################
		player1Text = (TextView) rootView
				.findViewById(R.id.TextView1);

		player1NoText = (TextView) rootView
				.findViewById(R.id.TextView09);

		if (ApplicationContextProvider.playerYC.get(0).equalsIgnoreCase(
				"1")) {
			player1Text.setTextColor(Color.YELLOW);
			player1NoText.setTextColor(Color.YELLOW);
		}
		
		if (ApplicationContextProvider.playerRC.get(0).equalsIgnoreCase(
				"1")) {
			player1Text.setTextColor(Color.RED);
			player1NoText.setTextColor(Color.RED);
		}

		player1NoText.setText(ApplicationContextProvider.playerNO.get(0));
		player1Text.setText(ApplicationContextProvider.playerSN.get(0));

		player1 = (ImageButton) rootView.findViewById(R.id.imageButton1);
		player1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (ApplicationContextProvider.playerRC.get(0)
						.equalsIgnoreCase("0")) {
					if (recPass.isChecked()) {
						pass.add(ApplicationContextProvider.playerSN.get(0)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(0) +")");
						Toast.makeText(
								getActivity(),
								"PASS - "
										+ApplicationContextProvider.playerSN.get(0)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(0) +")", Toast.LENGTH_SHORT)
								.show();
					}
					// GOAL
					if (goal.isChecked()) {
						// record just the goal as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());
						content.put("EventName", "Goal");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(0)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(0) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Goal
						if (recPass.isChecked()) 
							pass.add("Goal");
						
						Toast.makeText(
								getActivity(),
								"GOAL - "
										+ ApplicationContextProvider.playerSN.get(0)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(0) +")", Toast.LENGTH_SHORT)
								.show();
					}

					// LOST POSSESION
					if (possession.isChecked()) {
						// never as a general event
						if (recPass.isChecked()) 
							pass.add("Lost Possession");
						
						Toast.makeText(
								getActivity(),
								"LOST POSSESSION - "
										+ ApplicationContextProvider.playerSN.get(0)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(0) +")", Toast.LENGTH_SHORT)
								.show();
					}

					// CORNER
					if (corner.isChecked()) {
						// record just the Corner as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());
						content.put("EventName", "Corner");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(0)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(0) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Corner
						if (recPass.isChecked()) 
							pass.add("Corner");
						
						Toast.makeText(
								getActivity(),
								"CORNER - "
										+ ApplicationContextProvider.playerSN.get(0)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(0) +")", Toast.LENGTH_SHORT)
								.show();
					}

					// Red Card
					if (redCard.isChecked()) {
						// record just the Corner as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());
						content.put("EventName", "Red Card");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(0)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(0) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Red Card
						if (recPass.isChecked()) 
							pass.add("Red Card");
						
						// Disable player
						Toast.makeText(
								getActivity(),
								"RED CARD - "
										+ApplicationContextProvider.playerSN.get(0)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(0) +")", Toast.LENGTH_SHORT)
								.show();

						player1Text.setTextColor(Color.RED);
						player1NoText.setTextColor(Color.RED);
						ApplicationContextProvider.playerRC.set(0, "1");
						ApplicationContextProvider.teamSize --;
					}
					// Yellow Card
					if (yellowCard.isChecked()) {
						// record just the Yellow Card as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());

						content.put("EventName", "Yellow Card");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(0)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(0) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Yellow Card
						if (recPass.isChecked()) 
							pass.add("Yellow Card");

						Toast.makeText(
								getActivity(),
								"YELLOW CARD - "
										+ ApplicationContextProvider.playerSN.get(0)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(0) +")", Toast.LENGTH_SHORT)
								.show();
						player1Text.setTextColor(Color.YELLOW);
						player1NoText.setTextColor(Color.YELLOW);
						ApplicationContextProvider.playerYC.set(0, "1");
					}

					// Foul
					if (foul.isChecked()) {
						// record just the Foul as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());

						content.put("EventName", "Foul");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(0)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(0) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Foul
						if (recPass.isChecked()) 
							pass.add("Foul");

						Toast.makeText(
								getActivity(),
								"FOUL COMMITTED ON - "
										+ApplicationContextProvider.playerSN.get(0)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(0) +")", Toast.LENGTH_SHORT)
								.show();
					}

					// Committed Foul
					if (committedFoul.isChecked()) {
						// record just the Committed Foul as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());

						content.put("EventName", "Committed Foul");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(0)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(0) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Committed Foul
						if (recPass.isChecked()) 
							pass.add("Committed Foul");
						
						Toast.makeText(
								getActivity(),
								"COMMITTED FOUL - "
										+ ApplicationContextProvider.playerSN.get(0)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(0) +")", Toast.LENGTH_SHORT)
								.show();
					}

					// Out
					if (out.isChecked()) {
						// record just the Out as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());
						content.put("EventName", "Out");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(0)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(0) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Committed Foul
						if (recPass.isChecked()) 
							pass.add("Out");
						
						Toast.makeText(
								getActivity(),
								"OUT - "
										+ ApplicationContextProvider.playerSN.get(0)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(0) +")", Toast.LENGTH_SHORT)
								.show();
					}
					
					// OFFSIDE
					if (offside.isChecked()) {
						// record just the Out as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());
						content.put("EventName", "Offside");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(0)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(0) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Committed Foul
						if (recPass.isChecked()) 
							pass.add("Offside");
						
						Toast.makeText(
								getActivity(),
								"OFFSIDE - "
										+ ApplicationContextProvider.playerSN.get(0)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(0) +")", Toast.LENGTH_SHORT)
								.show();
					}
					
				} else {
					Toast.makeText(getActivity(),
							"This player has been eliminated by Red Card.",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		
		// #############    PLAYER 2   ##################
		player2Text = (TextView) rootView
				.findViewById(R.id.TextView2);

		player2NoText = (TextView) rootView
				.findViewById(R.id.TextView07);
		if (ApplicationContextProvider.playerYC.get(1).equalsIgnoreCase(
				"1")) {
			player2Text.setTextColor(Color.YELLOW);
			player2NoText.setTextColor(Color.YELLOW);
		}
		if (ApplicationContextProvider.playerRC.get(1).equalsIgnoreCase(
				"1")) {
			player2Text.setTextColor(Color.RED);
			player2NoText.setTextColor(Color.RED);
		}

		player2NoText.setText(ApplicationContextProvider.playerNO
				.get(1));
		player2Text.setText(ApplicationContextProvider.playerSN.get(1));

		player2 = (ImageButton) rootView.findViewById(R.id.ImageButton01);
		player2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (ApplicationContextProvider.playerRC.get(1)
						.equalsIgnoreCase("0")) {
					if (recPass.isChecked()) {
						pass.add(ApplicationContextProvider.playerSN.get(1)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(1) +")");
						Toast.makeText(
								getActivity(),
								"PASS - "
										+ApplicationContextProvider.playerSN.get(1)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(1) +")", Toast.LENGTH_SHORT)
								.show();
					}
					// GOAL
					if (goal.isChecked()) {
						// record just the goal as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());
						content.put("EventName", "Goal");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(1)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(1) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Goal
						if (recPass.isChecked()) 
							pass.add("Goal");
						
						Toast.makeText(
								getActivity(),
								"GOAL - "
										+ ApplicationContextProvider.playerSN.get(1)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(1) +")", Toast.LENGTH_SHORT)
								.show();
					}

					// LOST POSSESION
					if (possession.isChecked()) {
						// never as a general event
						if (recPass.isChecked()) 
							pass.add("Lost Possession");

						Toast.makeText(
								getActivity(),
								"LOST POSSESSION - "
										+ ApplicationContextProvider.playerSN.get(1)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(1) +")", Toast.LENGTH_SHORT)
								.show();
					}

					// CORNER
					if (corner.isChecked()) {
						// record just the Corner as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());
						content.put("EventName", "Corner");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(1)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(1) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Corner
						if (recPass.isChecked()) 
							pass.add("Corner");
						
						Toast.makeText(
								getActivity(),
								"CORNER - "
										+ ApplicationContextProvider.playerSN.get(1)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(1) +")", Toast.LENGTH_SHORT)
								.show();
					}

					// Red Card
					if (redCard.isChecked()) {
						// record just the Corner as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());
						content.put("EventName", "Red Card");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(1)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(1) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Red Card
						if (recPass.isChecked()) 
							pass.add("Red Card");
						
						// Disable player
						Toast.makeText(
								getActivity(),
								"RED CARD - "
										+ApplicationContextProvider.playerSN.get(1)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(1) +")", Toast.LENGTH_SHORT)
								.show();

						player2Text.setTextColor(Color.RED);
						player2NoText.setTextColor(Color.RED);
						ApplicationContextProvider.playerRC.set(1, "1");
						ApplicationContextProvider.teamSize --;


					}
					// Yellow Card
					if (yellowCard.isChecked()) {
						// record just the Yellow Card as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());

						content.put("EventName", "Yellow Card");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(1)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(1) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Yellow Card
						if (recPass.isChecked()) 
							pass.add("Yellow Card");

						Toast.makeText(
								getActivity(),
								"YELLOW CARD - "
										+ ApplicationContextProvider.playerSN.get(1)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(1) +")", Toast.LENGTH_SHORT)
								.show();
						player2Text.setTextColor(Color.YELLOW);
						player2NoText.setTextColor(Color.YELLOW);
						ApplicationContextProvider.playerYC.set(1, "1");
					}

					// Foul
					if (foul.isChecked()) {
						// record just the Foul as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());

						content.put("EventName", "Foul");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(1)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(1) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Foul
						if (recPass.isChecked()) 
							pass.add("Foul");

						Toast.makeText(
								getActivity(),
								"FOUL COMMITTED ON - "
										+ApplicationContextProvider.playerSN.get(1)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(1) +")", Toast.LENGTH_SHORT)
								.show();
					}

					// Committed Foul
					if (committedFoul.isChecked()) {
						// record just the Committed Foul as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());

						content.put("EventName", "Committed Foul");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(1)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(1) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Committed Foul
						if (recPass.isChecked()) 
							pass.add("Committed Foul");
						
						Toast.makeText(
								getActivity(),
								"COMMITTED FOUL - "
										+ ApplicationContextProvider.playerSN.get(1)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(1) +")", Toast.LENGTH_SHORT)
								.show();
					}

					// Out
					if (out.isChecked()) {
						// record just the Out as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());
						content.put("EventName", "Out");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(1)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(1) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Committed Foul
						if (recPass.isChecked()) 
							pass.add("Out");
						
						Toast.makeText(
								getActivity(),
								"OUT - "
										+ ApplicationContextProvider.playerSN.get(1)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(1) +")", Toast.LENGTH_SHORT)
								.show();
					}
					
					// OFFSIDE
					if (offside.isChecked()) {
						// record just the Out as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());
						content.put("EventName", "Offside");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(1)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(1) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Committed Foul
						if (recPass.isChecked()) 
							pass.add("Offside");
						
						Toast.makeText(
								getActivity(),
								"OFFSIDE - "
										+ ApplicationContextProvider.playerSN.get(1)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(1) +")", Toast.LENGTH_SHORT)
								.show();
					}
					
				} else {
					Toast.makeText(getActivity(),
							"This player has been eliminated by Red Card.",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		// ############   PLAYER 3     #######################
		player3Text = (TextView) rootView
				.findViewById(R.id.TextView3);

		player3NoText = (TextView) rootView
				.findViewById(R.id.TextView08);

		if (ApplicationContextProvider.playerYC.get(2).equalsIgnoreCase(
				"1")) {
			player3Text.setTextColor(Color.YELLOW);
			player3NoText.setTextColor(Color.YELLOW);
		}
		
		if (ApplicationContextProvider.playerRC.get(2).equalsIgnoreCase(
				"1")) {
			player3Text.setTextColor(Color.RED);
			player3NoText.setTextColor(Color.RED);
		}

		player3NoText.setText(ApplicationContextProvider.playerNO
				.get(2));
		player3Text.setText(ApplicationContextProvider.playerSN.get(2));

		player3 = (ImageButton) rootView.findViewById(R.id.ImageButton02);
		player3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (ApplicationContextProvider.playerRC.get(2)
						.equalsIgnoreCase("0")) {
					if (recPass.isChecked()) {
						pass.add(ApplicationContextProvider.playerSN.get(2)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(2) +")");
						Toast.makeText(
								getActivity(),
								"PASS - "
										+ApplicationContextProvider.playerSN.get(2)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(2) +")", Toast.LENGTH_SHORT)
								.show();
					}
					// GOAL
					if (goal.isChecked()) {
						// record just the goal as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());
						content.put("EventName", "Goal");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(2)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(2) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Goal
						if (recPass.isChecked()) 
							pass.add("Goal");
						
						Toast.makeText(
								getActivity(),
								"GOAL - "
										+ ApplicationContextProvider.playerSN.get(2)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(2) +")", Toast.LENGTH_SHORT)
								.show();
					}

					// LOST POSSESION
					if (possession.isChecked()) {
						// never as a general event
						if (recPass.isChecked()) 
							pass.add("Lost Possession");
						
						Toast.makeText(
								getActivity(),
								"LOST POSSESSION - "
										+ ApplicationContextProvider.playerSN.get(2)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(2) +")", Toast.LENGTH_SHORT)
								.show();
					}

					// CORNER
					if (corner.isChecked()) {
						// record just the Corner as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());
						content.put("EventName", "Corner");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(2)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(2) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Corner
						if (recPass.isChecked()) 
							pass.add("Corner");
						
						Toast.makeText(
								getActivity(),
								"CORNER - "
										+ ApplicationContextProvider.playerSN.get(2)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(2) +")", Toast.LENGTH_SHORT)
								.show();
					}

					// Red Card
					if (redCard.isChecked()) {
						// record just the Corner as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());
						content.put("EventName", "Red Card");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(2)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(2) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Red Card
						if (recPass.isChecked()) 
							pass.add("Red Card");
						
						// Disable player
						Toast.makeText(
								getActivity(),
								"RED CARD - "
										+ApplicationContextProvider.playerSN.get(2)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(2) +")", Toast.LENGTH_SHORT)
								.show();

						player3Text.setTextColor(Color.RED);
						player3NoText.setTextColor(Color.RED);
						ApplicationContextProvider.playerRC.set(2, "1");
						ApplicationContextProvider.teamSize --;
					}
					// Yellow Card
					if (yellowCard.isChecked()) {
						// record just the Yellow Card as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());

						content.put("EventName", "Yellow Card");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(2)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(2) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Yellow Card
						if (recPass.isChecked()) 
							pass.add("Yellow Card");

						Toast.makeText(
								getActivity(),
								"YELLOW CARD - "
										+ ApplicationContextProvider.playerSN.get(2)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(2) +")", Toast.LENGTH_SHORT)
								.show();
						player3Text.setTextColor(Color.YELLOW);
						player3NoText.setTextColor(Color.YELLOW);
						ApplicationContextProvider.playerYC.set(2, "1");
					}

					// Foul
					if (foul.isChecked()) {
						// record just the Foul as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());

						content.put("EventName", "Foul");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(2)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(2) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Foul
						if (recPass.isChecked()) 
							pass.add("Foul");

						Toast.makeText(
								getActivity(),
								"FOUL COMMITTED ON - "
										+ApplicationContextProvider.playerSN.get(2)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(2) +")", Toast.LENGTH_SHORT)
								.show();
					}

					// Committed Foul
					if (committedFoul.isChecked()) {
						// record just the Committed Foul as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());

						content.put("EventName", "Committed Foul");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(2)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(2) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Committed Foul
						if (recPass.isChecked()) 
							pass.add("Committed Foul");
						
						Toast.makeText(
								getActivity(),
								"COMMITTED FOUL - "
										+ ApplicationContextProvider.playerSN.get(2)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(2) +")", Toast.LENGTH_SHORT)
								.show();
					}

					// Out
					if (out.isChecked()) {
						// record just the Out as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());
						content.put("EventName", "Out");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(2)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(2) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Committed Foul
						if (recPass.isChecked()) 
							pass.add("Out");
						
						Toast.makeText(
								getActivity(),
								"OUT - "
										+ ApplicationContextProvider.playerSN.get(2)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(2) +")", Toast.LENGTH_SHORT)
								.show();
					}
					
					// OFFSIDE
					if (offside.isChecked()) {
						// record just the Out as a general event
						ContentValues content = new ContentValues();
						content.put("Minute",
								(String) StatsFragmentTab.getTime());
						content.put("EventName", "Offside");
						content.put(
								"PlayerName",
								ApplicationContextProvider.playerSN.get(2)
								+ " (No."
								+ ApplicationContextProvider.playerNO
										.get(2) +")");
						database = dbhelper.getWritableDatabase();
						database.insert("HomeTeamEvents", null, content);
						database.close();

						// record the Pass AND the Committed Foul
						if (recPass.isChecked()) 
							pass.add("Offside");
						
						Toast.makeText(
								getActivity(),
								"OFFSIDE - "
										+ ApplicationContextProvider.playerSN.get(2)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(2) +")", Toast.LENGTH_SHORT)
								.show();
					}
					
				} else {
					Toast.makeText(getActivity(),
							"This player has been eliminated by Red Card.",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		// ############   PLAYER 4     #######################
				player4Text = (TextView) rootView
						.findViewById(R.id.TextView4);

				player4NoText = (TextView) rootView
						.findViewById(R.id.TextView10);
				
				if (ApplicationContextProvider.playerYC.get(3).equalsIgnoreCase(
						"1")) {
					player4Text.setTextColor(Color.YELLOW);
					player4NoText.setTextColor(Color.YELLOW);
				}
				
				if (ApplicationContextProvider.playerRC.get(3).equalsIgnoreCase(
						"1")) {
					player4Text.setTextColor(Color.RED);
					player4NoText.setTextColor(Color.RED);
				}

				player4NoText.setText(ApplicationContextProvider.playerNO.get(3));
				player4Text.setText(ApplicationContextProvider.playerSN.get(3));

				player4 = (ImageButton) rootView.findViewById(R.id.ImageButton03);
				player4.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (ApplicationContextProvider.playerRC.get(3)
								.equalsIgnoreCase("0")) {
							if (recPass.isChecked()) {
								pass.add(ApplicationContextProvider.playerSN.get(3)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(3) +")");
								Toast.makeText(
										getActivity(),
										"PASS - "
												+ApplicationContextProvider.playerSN.get(3)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(3) +")", Toast.LENGTH_SHORT)
										.show();
							}
							// GOAL
							if (goal.isChecked()) {
								// record just the goal as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Goal");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(3)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(3) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Goal
								if (recPass.isChecked()) 
									pass.add("Goal");
								
								Toast.makeText(
										getActivity(),
										"GOAL - "
												+ ApplicationContextProvider.playerSN.get(3)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(3) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// LOST POSSESION
							if (possession.isChecked()) {
								// never as a general event
								if (recPass.isChecked()) 
									pass.add("Lost Possession");
								
								Toast.makeText(
										getActivity(),
										"LOST POSSESSION - "
												+ ApplicationContextProvider.playerSN.get(3)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(3) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// CORNER
							if (corner.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Corner");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(3)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(3) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Corner
								if (recPass.isChecked()) 
									pass.add("Corner");
								
								Toast.makeText(
										getActivity(),
										"CORNER - "
												+ ApplicationContextProvider.playerSN.get(3)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(3) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Red Card
							if (redCard.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Red Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(3)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(3) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Red Card
								if (recPass.isChecked()) 
									pass.add("Red Card");
								
								// Disable player
								Toast.makeText(
										getActivity(),
										"RED CARD - "
												+ApplicationContextProvider.playerSN.get(3)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(3) +")", Toast.LENGTH_SHORT)
										.show();

								player4Text.setTextColor(Color.RED);
								player4NoText.setTextColor(Color.RED);
								ApplicationContextProvider.playerRC.set(3, "1");
								ApplicationContextProvider.teamSize --;
							}
							// Yellow Card
							if (yellowCard.isChecked()) {
								// record just the Yellow Card as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Yellow Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(3)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(3) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Yellow Card
								if (recPass.isChecked()) 
									pass.add("Yellow Card");

								Toast.makeText(
										getActivity(),
										"YELLOW CARD - "
												+ ApplicationContextProvider.playerSN.get(3)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(3) +")", Toast.LENGTH_SHORT)
										.show();
								player4Text.setTextColor(Color.YELLOW);
								player4NoText.setTextColor(Color.YELLOW);
								ApplicationContextProvider.playerYC.set(3, "1");
							}

							// Foul
							if (foul.isChecked()) {
								// record just the Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(3)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(3) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Foul
								if (recPass.isChecked()) 
									pass.add("Foul");

								Toast.makeText(
										getActivity(),
										"FOUL COMMITTED ON - "
												+ApplicationContextProvider.playerSN.get(3)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(3) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Committed Foul
							if (committedFoul.isChecked()) {
								// record just the Committed Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Committed Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(3)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(3) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Committed Foul");
								
								Toast.makeText(
										getActivity(),
										"COMMITTED FOUL - "
												+ ApplicationContextProvider.playerSN.get(3)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(3) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Out
							if (out.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Out");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(3)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(3) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Out");
								
								Toast.makeText(
										getActivity(),
										"OUT - "
												+ ApplicationContextProvider.playerSN.get(3)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(3) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
							// OFFSIDE
							if (offside.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Offside");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(3)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(3) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Offside");
								
								Toast.makeText(
										getActivity(),
										"OFFSIDE - "
												+ ApplicationContextProvider.playerSN.get(3)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(3) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
						} else {
							Toast.makeText(getActivity(),
									"This player has been eliminated by Red Card.",
									Toast.LENGTH_SHORT).show();
						}
					}
				});

		
				// ############   PLAYER 5     #######################
				player5Text = (TextView) rootView
						.findViewById(R.id.TextView5);

				player5NoText = (TextView) rootView
						.findViewById(R.id.TextView11);
				if (ApplicationContextProvider.playerYC.get(4).equalsIgnoreCase(
						"1")) {
					player5Text.setTextColor(Color.YELLOW);
					player5NoText.setTextColor(Color.YELLOW);
				}
				
				if (ApplicationContextProvider.playerRC.get(4).equalsIgnoreCase(
						"1")) {
					player5Text.setTextColor(Color.RED);
					player5NoText.setTextColor(Color.RED);
				}

				player5NoText.setText(ApplicationContextProvider.playerNO.get(4));
				player5Text.setText(ApplicationContextProvider.playerSN.get(4));

				player5 = (ImageButton) rootView.findViewById(R.id.ImageButton04);
				player5.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (ApplicationContextProvider.playerRC.get(4)
								.equalsIgnoreCase("0")) {
							if (recPass.isChecked()) {
								pass.add(ApplicationContextProvider.playerSN.get(4)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(4) +")");
								Toast.makeText(
										getActivity(),
										"PASS - "
												+ApplicationContextProvider.playerSN.get(4)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(4) +")", Toast.LENGTH_SHORT)
										.show();
							}
							// GOAL
							if (goal.isChecked()) {
								// record just the goal as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Goal");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(4)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(4) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Goal
								if (recPass.isChecked()) 
									pass.add("Goal");
								
								Toast.makeText(
										getActivity(),
										"GOAL - "
												+ ApplicationContextProvider.playerSN.get(4)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(4) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// LOST POSSESION
							if (possession.isChecked()) {
								// never as a general event
								if (recPass.isChecked()) 
									pass.add("Lost Possession");
								
								Toast.makeText(
										getActivity(),
										"LOST POSSESSION - "
												+ ApplicationContextProvider.playerSN.get(4)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(4) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// CORNER
							if (corner.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Corner");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(4)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(4) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Corner
								if (recPass.isChecked()) 
									pass.add("Corner");
								
								Toast.makeText(
										getActivity(),
										"CORNER - "
												+ ApplicationContextProvider.playerSN.get(4)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(4) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Red Card
							if (redCard.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Red Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(4)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(4) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Red Card
								if (recPass.isChecked()) 
									pass.add("Red Card");
								
								// Disable player
								Toast.makeText(
										getActivity(),
										"RED CARD - "
												+ApplicationContextProvider.playerSN.get(4)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(4) +")", Toast.LENGTH_SHORT)
										.show();

								player5Text.setTextColor(Color.RED);
								player5NoText.setTextColor(Color.RED);
								ApplicationContextProvider.playerRC.set(4, "1");
								ApplicationContextProvider.teamSize --;
							}
							// Yellow Card
							if (yellowCard.isChecked()) {
								// record just the Yellow Card as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Yellow Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(4)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(4) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Yellow Card
								if (recPass.isChecked()) 
									pass.add("Yellow Card");

								Toast.makeText(
										getActivity(),
										"YELLOW CARD - "
												+ ApplicationContextProvider.playerSN.get(4)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(4) +")", Toast.LENGTH_SHORT)
										.show();
								player5Text.setTextColor(Color.YELLOW);
								player5NoText.setTextColor(Color.YELLOW);
								ApplicationContextProvider.playerYC.set(4, "1");
							}

							// Foul
							if (foul.isChecked()) {
								// record just the Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(4)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(4) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Foul
								if (recPass.isChecked()) 
									pass.add("Foul");

								Toast.makeText(
										getActivity(),
										"FOUL COMMITTED ON - "
												+ApplicationContextProvider.playerSN.get(4)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(4) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Committed Foul
							if (committedFoul.isChecked()) {
								// record just the Committed Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Committed Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(4)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(4) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Committed Foul");
								
								Toast.makeText(
										getActivity(),
										"COMMITTED FOUL - "
												+ ApplicationContextProvider.playerSN.get(4)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(4) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Out
							if (out.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Out");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(4)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(4) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Out");
								
								Toast.makeText(
										getActivity(),
										"OUT - "
												+ ApplicationContextProvider.playerSN.get(4)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(4) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
							// OFFSIDE
							if (offside.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Offside");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(4)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(4) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Offside");
								
								Toast.makeText(
										getActivity(),
										"OFFSIDE - "
												+ ApplicationContextProvider.playerSN.get(4)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(4) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
						} else {
							Toast.makeText(getActivity(),
									"This player has been eliminated by Red Card.",
									Toast.LENGTH_SHORT).show();
						}
					}
				});

				// ############   PLAYER 6     #######################
				player6Text = (TextView) rootView
						.findViewById(R.id.TextView01);

				player6NoText = (TextView) rootView
						.findViewById(R.id.textView6);

				if (ApplicationContextProvider.playerYC.get(5).equalsIgnoreCase(
						"1")) {
					player6Text.setTextColor(Color.YELLOW);
					player6NoText.setTextColor(Color.YELLOW);
				}
				
				if (ApplicationContextProvider.playerRC.get(5).equalsIgnoreCase(
						"1")) {
					player6Text.setTextColor(Color.RED);
					player6NoText.setTextColor(Color.RED);
				}

				player6NoText.setText(ApplicationContextProvider.playerNO.get(5));
				player6Text.setText(ApplicationContextProvider.playerSN.get(5));

				player6 = (ImageButton) rootView.findViewById(R.id.ImageButton05);
				player6.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (ApplicationContextProvider.playerRC.get(5)
								.equalsIgnoreCase("0")) {
							if (recPass.isChecked()) {
								pass.add(ApplicationContextProvider.playerSN.get(5)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(5) +")");
								Toast.makeText(
										getActivity(),
										"PASS - "
												+ApplicationContextProvider.playerSN.get(5)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(5) +")", Toast.LENGTH_SHORT)
										.show();
							}
							// GOAL
							if (goal.isChecked()) {
								// record just the goal as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Goal");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(5)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(5) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Goal
								if (recPass.isChecked()) 
									pass.add("Goal");
								
								Toast.makeText(
										getActivity(),
										"GOAL - "
												+ ApplicationContextProvider.playerSN.get(5)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(5) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// LOST POSSESION
							if (possession.isChecked()) {
								// never as a general event
								if (recPass.isChecked()) 
									pass.add("Lost Possession");
								
								Toast.makeText(
										getActivity(),
										"LOST POSSESSION - "
												+ ApplicationContextProvider.playerSN.get(5)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(5) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// CORNER
							if (corner.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Corner");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(5)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(5) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Corner
								if (recPass.isChecked()) 
									pass.add("Corner");
								
								Toast.makeText(
										getActivity(),
										"CORNER - "
												+ ApplicationContextProvider.playerSN.get(5)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(5) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Red Card
							if (redCard.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Red Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(5)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(5) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Red Card
								if (recPass.isChecked()) 
									pass.add("Red Card");
								
								// Disable player
								Toast.makeText(
										getActivity(),
										"RED CARD - "
												+ApplicationContextProvider.playerSN.get(5)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(5) +")", Toast.LENGTH_SHORT)
										.show();

								player6Text.setTextColor(Color.RED);
								player6NoText.setTextColor(Color.RED);
								ApplicationContextProvider.playerRC.set(5, "1");
								ApplicationContextProvider.teamSize --;
							}
							// Yellow Card
							if (yellowCard.isChecked()) {
								// record just the Yellow Card as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Yellow Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(5)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(5) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Yellow Card
								if (recPass.isChecked()) 
									pass.add("Yellow Card");

								Toast.makeText(
										getActivity(),
										"YELLOW CARD - "
												+ ApplicationContextProvider.playerSN.get(5)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(5) +")", Toast.LENGTH_SHORT)
										.show();
								player6Text.setTextColor(Color.YELLOW);
								player6NoText.setTextColor(Color.YELLOW);
								ApplicationContextProvider.playerYC.set(5, "1");
							}

							// Foul
							if (foul.isChecked()) {
								// record just the Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(5)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(5) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Foul
								if (recPass.isChecked()) 
									pass.add("Foul");

								Toast.makeText(
										getActivity(),
										"FOUL COMMITTED ON - "
												+ApplicationContextProvider.playerSN.get(5)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(5) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Committed Foul
							if (committedFoul.isChecked()) {
								// record just the Committed Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Committed Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(5)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(5) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Committed Foul");
								
								Toast.makeText(
										getActivity(),
										"COMMITTED FOUL - "
												+ ApplicationContextProvider.playerSN.get(5)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(5) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Out
							if (out.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Out");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(5)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(5) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Out");
								
								Toast.makeText(
										getActivity(),
										"OUT - "
												+ ApplicationContextProvider.playerSN.get(5)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(5) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
							// OFFSIDE
							if (offside.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Offside");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(5)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(5) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Offside");
								
								Toast.makeText(
										getActivity(),
										"OFFSIDE - "
												+ ApplicationContextProvider.playerSN.get(5)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(5) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
						} else {
							Toast.makeText(getActivity(),
									"This player has been eliminated by Red Card.",
									Toast.LENGTH_SHORT).show();
						}
					}
				});
				
				// ############   PLAYER 7     #######################
				player7Text = (TextView) rootView
						.findViewById(R.id.TextView02);

				player7NoText = (TextView) rootView
						.findViewById(R.id.TextView12);

				if (ApplicationContextProvider.playerYC.get(6).equalsIgnoreCase(
						"1")) {
					player7Text.setTextColor(Color.YELLOW);
					player7NoText.setTextColor(Color.YELLOW);
				}
				
				if (ApplicationContextProvider.playerRC.get(6).equalsIgnoreCase(
						"1")) {
					player7Text.setTextColor(Color.RED);
					player7NoText.setTextColor(Color.RED);
				}

				player7NoText.setText(ApplicationContextProvider.playerNO.get(6));
				player7Text.setText(ApplicationContextProvider.playerSN.get(6));

				player7 = (ImageButton) rootView.findViewById(R.id.ImageButton06);
				player7.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (ApplicationContextProvider.playerRC.get(6)
								.equalsIgnoreCase("0")) {
							if (recPass.isChecked()) {
								pass.add(ApplicationContextProvider.playerSN.get(6)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(6) +")");
								Toast.makeText(
										getActivity(),
										"PASS - "
												+ApplicationContextProvider.playerSN.get(6)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(6) +")", Toast.LENGTH_SHORT)
										.show();
							}
							// GOAL
							if (goal.isChecked()) {
								// record just the goal as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Goal");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(6)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(6) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Goal
								if (recPass.isChecked()) 
									pass.add("Goal");
								
								Toast.makeText(
										getActivity(),
										"GOAL - "
												+ ApplicationContextProvider.playerSN.get(6)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(6) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// LOST POSSESION
							if (possession.isChecked()) {
								// never as a general event
								if (recPass.isChecked()) 
									pass.add("Lost Possession");
								
								Toast.makeText(
										getActivity(),
										"LOST POSSESSION - "
												+ ApplicationContextProvider.playerSN.get(6)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(6) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// CORNER
							if (corner.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Corner");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(6)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(6) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Corner
								if (recPass.isChecked()) 
									pass.add("Corner");
								
								Toast.makeText(
										getActivity(),
										"CORNER - "
												+ ApplicationContextProvider.playerSN.get(6)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(6) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Red Card
							if (redCard.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Red Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(6)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(6) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Red Card
								if (recPass.isChecked()) 
									pass.add("Red Card");
								
								// Disable player
								Toast.makeText(
										getActivity(),
										"RED CARD - "
												+ApplicationContextProvider.playerSN.get(6)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(6) +")", Toast.LENGTH_SHORT)
										.show();

								player7Text.setTextColor(Color.RED);
								player7NoText.setTextColor(Color.RED);
								ApplicationContextProvider.playerRC.set(6, "1");
								ApplicationContextProvider.teamSize --;
							}
							// Yellow Card
							if (yellowCard.isChecked()) {
								// record just the Yellow Card as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Yellow Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(6)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(6) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Yellow Card
								if (recPass.isChecked()) 
									pass.add("Yellow Card");

								Toast.makeText(
										getActivity(),
										"YELLOW CARD - "
												+ ApplicationContextProvider.playerSN.get(6)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(6) +")", Toast.LENGTH_SHORT)
										.show();
								player7Text.setTextColor(Color.YELLOW);
								player7NoText.setTextColor(Color.YELLOW);
								ApplicationContextProvider.playerYC.set(6, "1");
							}

							// Foul
							if (foul.isChecked()) {
								// record just the Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(6)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(6) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Foul
								if (recPass.isChecked()) 
									pass.add("Foul");

								Toast.makeText(
										getActivity(),
										"FOUL COMMITTED ON - "
												+ApplicationContextProvider.playerSN.get(6)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(6) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Committed Foul
							if (committedFoul.isChecked()) {
								// record just the Committed Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Committed Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(6)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(6) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Committed Foul");
								
								Toast.makeText(
										getActivity(),
										"COMMITTED FOUL - "
												+ ApplicationContextProvider.playerSN.get(6)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(6) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Out
							if (out.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Out");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(6)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(6) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Out");
								
								Toast.makeText(
										getActivity(),
										"OUT - "
												+ ApplicationContextProvider.playerSN.get(6)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(6) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
							// OFFSIDE
							if (offside.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Offside");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(6)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(6) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Offside");
								
								Toast.makeText(
										getActivity(),
										"OFFSIDE - "
												+ ApplicationContextProvider.playerSN.get(6)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(6) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
						} else {
							Toast.makeText(getActivity(),
									"This player has been eliminated by Red Card.",
									Toast.LENGTH_SHORT).show();
						}
					}
				});


				// ############   PLAYER 8     #######################
				player8Text = (TextView) rootView
						.findViewById(R.id.TextView03);

				player8NoText = (TextView) rootView
						.findViewById(R.id.TextView13);

				if (ApplicationContextProvider.playerYC.get(7).equalsIgnoreCase(
						"1")) {
					player8Text.setTextColor(Color.YELLOW);
					player8NoText.setTextColor(Color.YELLOW);
				}
				
				if (ApplicationContextProvider.playerRC.get(7).equalsIgnoreCase(
						"1")) {
					player8Text.setTextColor(Color.RED);
					player8NoText.setTextColor(Color.RED);
				}

				player8NoText.setText(ApplicationContextProvider.playerNO.get(7));
				player8Text.setText(ApplicationContextProvider.playerSN.get(7));

				player8 = (ImageButton) rootView.findViewById(R.id.ImageButton07);
				player8.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (ApplicationContextProvider.playerRC.get(7)
								.equalsIgnoreCase("0")) {
							if (recPass.isChecked()) {
								pass.add(ApplicationContextProvider.playerSN.get(7)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(7) +")");
								Toast.makeText(
										getActivity(),
										"PASS - "
												+ApplicationContextProvider.playerSN.get(7)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(7) +")", Toast.LENGTH_SHORT)
										.show();
							}
							// GOAL
							if (goal.isChecked()) {
								// record just the goal as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Goal");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(7)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(7) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Goal
								if (recPass.isChecked()) 
									pass.add("Goal");
								
								Toast.makeText(
										getActivity(),
										"GOAL - "
												+ ApplicationContextProvider.playerSN.get(7)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(7) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// LOST POSSESION
							if (possession.isChecked()) {
								// never as a general event
								if (recPass.isChecked()) 
									pass.add("Lost Possession");
								
								Toast.makeText(
										getActivity(),
										"LOST POSSESSION - "
												+ ApplicationContextProvider.playerSN.get(7)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(7) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// CORNER
							if (corner.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Corner");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(7)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(7) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Corner
								if (recPass.isChecked()) 
									pass.add("Corner");
								
								Toast.makeText(
										getActivity(),
										"CORNER - "
												+ ApplicationContextProvider.playerSN.get(7)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(7) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Red Card
							if (redCard.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Red Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(7)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(7) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Red Card
								if (recPass.isChecked()) 
									pass.add("Red Card");
								
								// Disable player
								Toast.makeText(
										getActivity(),
										"RED CARD - "
												+ApplicationContextProvider.playerSN.get(7)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(7) +")", Toast.LENGTH_SHORT)
										.show();

								player8Text.setTextColor(Color.RED);
								player8NoText.setTextColor(Color.RED);
								ApplicationContextProvider.playerRC.set(7, "1");
								ApplicationContextProvider.teamSize --;
							}
							// Yellow Card
							if (yellowCard.isChecked()) {
								// record just the Yellow Card as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Yellow Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(7)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(7) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Yellow Card
								if (recPass.isChecked()) 
									pass.add("Yellow Card");

								Toast.makeText(
										getActivity(),
										"YELLOW CARD - "
												+ ApplicationContextProvider.playerSN.get(7)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(7) +")", Toast.LENGTH_SHORT)
										.show();
								player8Text.setTextColor(Color.YELLOW);
								player8NoText.setTextColor(Color.YELLOW);
								ApplicationContextProvider.playerYC.set(7, "1");
							}

							// Foul
							if (foul.isChecked()) {
								// record just the Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(7)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(7) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Foul
								if (recPass.isChecked()) 
									pass.add("Foul");

								Toast.makeText(
										getActivity(),
										"FOUL COMMITTED ON - "
												+ApplicationContextProvider.playerSN.get(7)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(7) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Committed Foul
							if (committedFoul.isChecked()) {
								// record just the Committed Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Committed Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(7)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(7) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Committed Foul");
								
								Toast.makeText(
										getActivity(),
										"COMMITTED FOUL - "
												+ ApplicationContextProvider.playerSN.get(7)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(7) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Out
							if (out.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Out");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(7)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(7) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Out");
								
								Toast.makeText(
										getActivity(),
										"OUT - "
												+ ApplicationContextProvider.playerSN.get(7)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(7) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
							// OFFSIDE
							if (offside.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Offside");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(7)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(7) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Offside");
								
								Toast.makeText(
										getActivity(),
										"OFFSIDE - "
												+ ApplicationContextProvider.playerSN.get(7)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(7) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
						} else {
							Toast.makeText(getActivity(),
									"This player has been eliminated by Red Card.",
									Toast.LENGTH_SHORT).show();
						}
					}
				});


				// ############   PLAYER 9     #######################
				player9Text = (TextView) rootView
						.findViewById(R.id.TextView04);

				player9NoText = (TextView) rootView
						.findViewById(R.id.TextView14);

				if (ApplicationContextProvider.playerYC.get(8).equalsIgnoreCase(
						"1")) {
					player9Text.setTextColor(Color.YELLOW);
					player9NoText.setTextColor(Color.YELLOW);
				}
				
				if (ApplicationContextProvider.playerRC.get(8).equalsIgnoreCase(
						"1")) {
					player9Text.setTextColor(Color.RED);
					player9NoText.setTextColor(Color.RED);
				}

				player9NoText.setText(ApplicationContextProvider.playerNO.get(8));
				player9Text.setText(ApplicationContextProvider.playerSN.get(8));

				player9 = (ImageButton) rootView.findViewById(R.id.ImageButton08);
				player9.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (ApplicationContextProvider.playerRC.get(8)
								.equalsIgnoreCase("0")) {
							if (recPass.isChecked()) {
								pass.add(ApplicationContextProvider.playerSN.get(8)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(8) +")");
								Toast.makeText(
										getActivity(),
										"PASS - "
												+ApplicationContextProvider.playerSN.get(8)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(8) +")", Toast.LENGTH_SHORT)
										.show();
							}
							// GOAL
							if (goal.isChecked()) {
								// record just the goal as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Goal");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(8)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(8) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Goal
								if (recPass.isChecked()) 
									pass.add("Goal");
								
								Toast.makeText(
										getActivity(),
										"GOAL - "
												+ ApplicationContextProvider.playerSN.get(8)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(8) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// LOST POSSESION
							if (possession.isChecked()) {
								// never as a general event
								if (recPass.isChecked()) 
									pass.add("Lost Possession");
								
								Toast.makeText(
										getActivity(),
										"LOST POSSESSION - "
												+ ApplicationContextProvider.playerSN.get(8)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(8) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// CORNER
							if (corner.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Corner");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(8)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(8) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Corner
								if (recPass.isChecked()) 
									pass.add("Corner");
								
								Toast.makeText(
										getActivity(),
										"CORNER - "
												+ ApplicationContextProvider.playerSN.get(8)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(8) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Red Card
							if (redCard.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Red Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(8)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(8) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Red Card
								if (recPass.isChecked()) 
									pass.add("Red Card");
								
								// Disable player
								Toast.makeText(
										getActivity(),
										"RED CARD - "
												+ApplicationContextProvider.playerSN.get(8)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(8) +")", Toast.LENGTH_SHORT)
										.show();

								player9Text.setTextColor(Color.RED);
								player9NoText.setTextColor(Color.RED);
								ApplicationContextProvider.playerRC.set(8, "1");
								ApplicationContextProvider.teamSize --;
							}
							// Yellow Card
							if (yellowCard.isChecked()) {
								// record just the Yellow Card as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Yellow Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(8)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(8) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Yellow Card
								if (recPass.isChecked()) 
									pass.add("Yellow Card");

								Toast.makeText(
										getActivity(),
										"YELLOW CARD - "
												+ ApplicationContextProvider.playerSN.get(8)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(8) +")", Toast.LENGTH_SHORT)
										.show();
								player9Text.setTextColor(Color.YELLOW);
								player9NoText.setTextColor(Color.YELLOW);
								ApplicationContextProvider.playerYC.set(8, "1");
							}

							// Foul
							if (foul.isChecked()) {
								// record just the Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(8)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(8) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Foul
								if (recPass.isChecked()) 
									pass.add("Foul");

								Toast.makeText(
										getActivity(),
										"FOUL COMMITTED ON - "
												+ApplicationContextProvider.playerSN.get(8)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(8) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Committed Foul
							if (committedFoul.isChecked()) {
								// record just the Committed Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Committed Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(8)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(8) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Committed Foul");
								
								Toast.makeText(
										getActivity(),
										"COMMITTED FOUL - "
												+ ApplicationContextProvider.playerSN.get(8)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(8) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Out
							if (out.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Out");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(8)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(8) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Out");
								
								Toast.makeText(
										getActivity(),
										"OUT - "
												+ ApplicationContextProvider.playerSN.get(8)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(8) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
							// OFFSIDE
							if (offside.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Offside");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(8)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(8) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Offside");
								
								Toast.makeText(
										getActivity(),
										"OFFSIDE - "
												+ ApplicationContextProvider.playerSN.get(8)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(8) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
						} else {
							Toast.makeText(getActivity(),
									"This player has been eliminated by Red Card.",
									Toast.LENGTH_SHORT).show();
						}
					}
				});


				// ############   PLAYER 10     #######################
				player10Text = (TextView) rootView
						.findViewById(R.id.TextView05);

				player10NoText = (TextView) rootView
						.findViewById(R.id.TextView15);

				if (ApplicationContextProvider.playerYC.get(9).equalsIgnoreCase(
						"1")) {
					player10Text.setTextColor(Color.YELLOW);
					player10NoText.setTextColor(Color.YELLOW);
				}
				
				if (ApplicationContextProvider.playerRC.get(9).equalsIgnoreCase(
						"1")) {
					player10Text.setTextColor(Color.RED);
					player10NoText.setTextColor(Color.RED);
				}

				player10NoText.setText(ApplicationContextProvider.playerNO.get(9));
				player10Text.setText(ApplicationContextProvider.playerSN.get(9));

				player10 = (ImageButton) rootView.findViewById(R.id.ImageButton09);
				player10.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (ApplicationContextProvider.playerRC.get(9)
								.equalsIgnoreCase("0")) {
							if (recPass.isChecked()) {
								pass.add(ApplicationContextProvider.playerSN.get(9)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(9) +")");
								Toast.makeText(
										getActivity(),
										"PASS - "
												+ApplicationContextProvider.playerSN.get(9)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(9) +")", Toast.LENGTH_SHORT)
										.show();
							}
							// GOAL
							if (goal.isChecked()) {
								// record just the goal as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Goal");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(9)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(9) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Goal
								if (recPass.isChecked()) 
									pass.add("Goal");
								
								Toast.makeText(
										getActivity(),
										"GOAL - "
												+ ApplicationContextProvider.playerSN.get(9)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(9) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// LOST POSSESION
							if (possession.isChecked()) {
								// never as a general event
								if (recPass.isChecked()) 
									pass.add("Lost Possession");
								
								Toast.makeText(
										getActivity(),
										"LOST POSSESSION - "
												+ ApplicationContextProvider.playerSN.get(9)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(9) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// CORNER
							if (corner.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Corner");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(9)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(9) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Corner
								if (recPass.isChecked()) 
									pass.add("Corner");
								
								Toast.makeText(
										getActivity(),
										"CORNER - "
												+ ApplicationContextProvider.playerSN.get(9)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(9) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Red Card
							if (redCard.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Red Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(9)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(9) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Red Card
								if (recPass.isChecked()) 
									pass.add("Red Card");
								
								// Disable player
								Toast.makeText(
										getActivity(),
										"RED CARD - "
												+ApplicationContextProvider.playerSN.get(9)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(9) +")", Toast.LENGTH_SHORT)
										.show();

								player10Text.setTextColor(Color.RED);
								player10NoText.setTextColor(Color.RED);
								ApplicationContextProvider.playerRC.set(9, "1");
								ApplicationContextProvider.teamSize --;
								player10.setVisibility(View.GONE);
								
							}
							// Yellow Card
							if (yellowCard.isChecked()) {
								// record just the Yellow Card as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Yellow Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(9)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(9) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Yellow Card
								if (recPass.isChecked()) 
									pass.add("Yellow Card");

								Toast.makeText(
										getActivity(),
										"YELLOW CARD - "
												+ ApplicationContextProvider.playerSN.get(9)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(9) +")", Toast.LENGTH_SHORT)
										.show();
								player10Text.setTextColor(Color.YELLOW);
								player10NoText.setTextColor(Color.YELLOW);
								ApplicationContextProvider.playerYC.set(9, "1");
							}

							// Foul
							if (foul.isChecked()) {
								// record just the Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(9)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(9) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Foul
								if (recPass.isChecked()) 
									pass.add("Foul");

								Toast.makeText(
										getActivity(),
										"FOUL COMMITTED ON - "
												+ApplicationContextProvider.playerSN.get(9)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(9) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Committed Foul
							if (committedFoul.isChecked()) {
								// record just the Committed Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Committed Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(9)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(9) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Committed Foul");
								
								Toast.makeText(
										getActivity(),
										"COMMITTED FOUL - "
												+ ApplicationContextProvider.playerSN.get(9)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(9) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Out
							if (out.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Out");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(9)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(9) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Out");
								
								Toast.makeText(
										getActivity(),
										"OUT - "
												+ ApplicationContextProvider.playerSN.get(9)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(9) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
							// OFFSIDE
							if (offside.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Offside");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(9)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(9) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Offside");
								
								Toast.makeText(
										getActivity(),
										"OFFSIDE - "
												+ ApplicationContextProvider.playerSN.get(9)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(9) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
						} else {
							Toast.makeText(getActivity(),
									"This player has been eliminated by Red Card.",
									Toast.LENGTH_SHORT).show();
						}
					}
				});


				// ############   PLAYER 11     #######################
				player11Text = (TextView) rootView
						.findViewById(R.id.TextView06);

				player11NoText = (TextView) rootView
						.findViewById(R.id.TextView16);

				if (ApplicationContextProvider.playerYC.get(10).equalsIgnoreCase(
						"1")) {
					player11Text.setTextColor(Color.YELLOW);
					player11NoText.setTextColor(Color.YELLOW);
				}
				
				if (ApplicationContextProvider.playerRC.get(10).equalsIgnoreCase(
						"1")) {
					player11Text.setTextColor(Color.RED);
					player11NoText.setTextColor(Color.RED);
				}

				player11NoText.setText(ApplicationContextProvider.playerNO.get(10));
				player11Text.setText(ApplicationContextProvider.playerSN.get(10));

				player11 = (ImageButton) rootView.findViewById(R.id.ImageButton10);
				player11.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (ApplicationContextProvider.playerRC.get(10)
								.equalsIgnoreCase("0")) {
							if (recPass.isChecked()) {
								pass.add(ApplicationContextProvider.playerSN.get(10)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(10) +")");
								Toast.makeText(
										getActivity(),
										"PASS - "
												+ApplicationContextProvider.playerSN.get(10)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(10) +")", Toast.LENGTH_SHORT)
										.show();
							}
							// GOAL
							if (goal.isChecked()) {
								// record just the goal as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Goal");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(10)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(10) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Goal
								if (recPass.isChecked()) 
									pass.add("Goal");
								
								Toast.makeText(
										getActivity(),
										"GOAL - "
												+ ApplicationContextProvider.playerSN.get(10)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(10) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// LOST POSSESION
							if (possession.isChecked()) {
								// never as a general event
								if (recPass.isChecked()) 
									pass.add("Lost Possession");
								
								Toast.makeText(
										getActivity(),
										"LOST POSSESSION - "
												+ ApplicationContextProvider.playerSN.get(10)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(10) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// CORNER
							if (corner.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Corner");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(10)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(10) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Corner
								if (recPass.isChecked()) 
									pass.add("Corner");
								
								Toast.makeText(
										getActivity(),
										"CORNER - "
												+ ApplicationContextProvider.playerSN.get(10)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(10) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Red Card
							if (redCard.isChecked()) {
								// record just the Corner as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Red Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(10)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(10) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Red Card
								if (recPass.isChecked()) 
									pass.add("Red Card");
								
								// Disable player
								Toast.makeText(
										getActivity(),
										"RED CARD - "
												+ApplicationContextProvider.playerSN.get(10)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(10) +")", Toast.LENGTH_SHORT)
										.show();

								player11Text.setTextColor(Color.RED);
								player11NoText.setTextColor(Color.RED);
								ApplicationContextProvider.playerRC.set(10, "1");
								ApplicationContextProvider.teamSize --;
							}
							// Yellow Card
							if (yellowCard.isChecked()) {
								// record just the Yellow Card as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Yellow Card");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(10)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(10) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Yellow Card
								if (recPass.isChecked()) 
									pass.add("Yellow Card");

								Toast.makeText(
										getActivity(),
										"YELLOW CARD - "
												+ ApplicationContextProvider.playerSN.get(10)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(10) +")", Toast.LENGTH_SHORT)
										.show();
								player11Text.setTextColor(Color.YELLOW);
								player11NoText.setTextColor(Color.YELLOW);
								ApplicationContextProvider.playerYC.set(10, "1");
							}

							// Foul
							if (foul.isChecked()) {
								// record just the Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(10)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(10) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Foul
								if (recPass.isChecked()) 
									pass.add("Foul");

								Toast.makeText(
										getActivity(),
										"FOUL COMMITTED ON - "
												+ApplicationContextProvider.playerSN.get(10)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(10) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Committed Foul
							if (committedFoul.isChecked()) {
								// record just the Committed Foul as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());

								content.put("EventName", "Committed Foul");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(10)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(10) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Committed Foul");
								
								Toast.makeText(
										getActivity(),
										"COMMITTED FOUL - "
												+ ApplicationContextProvider.playerSN.get(10)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(10) +")", Toast.LENGTH_SHORT)
										.show();
							}

							// Out
							if (out.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Out");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(10)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(10) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Out");
								
								Toast.makeText(
										getActivity(),
										"OUT - "
												+ ApplicationContextProvider.playerSN.get(10)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(10) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
							// OFFSIDE
							if (offside.isChecked()) {
								// record just the Out as a general event
								ContentValues content = new ContentValues();
								content.put("Minute",
										(String) StatsFragmentTab.getTime());
								content.put("EventName", "Offside");
								content.put(
										"PlayerName",
										ApplicationContextProvider.playerSN.get(10)
										+ " (No."
										+ ApplicationContextProvider.playerNO
												.get(10) +")");
								database = dbhelper.getWritableDatabase();
								database.insert("HomeTeamEvents", null, content);
								database.close();

								// record the Pass AND the Committed Foul
								if (recPass.isChecked()) 
									pass.add("Offside");
								
								Toast.makeText(
										getActivity(),
										"OFFSIDE - "
												+ ApplicationContextProvider.playerSN.get(10)
												+ " (No."
												+ ApplicationContextProvider.playerNO
														.get(10) +")", Toast.LENGTH_SHORT)
										.show();
							}
							
						} else {
							Toast.makeText(getActivity(),
									"This player has been eliminated by Red Card.",
									Toast.LENGTH_SHORT).show();
						}
					}
				});


		return rootView;
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		
		// substitute current player (transmitted through lastContextMenuButton)
		// with the chosen sub (transmitted through item) 
		
		// the player in the filed denoted by lastContextMenuButton
		// is removed from the first 11 on the field 
		
		lastContextMenuButton.getTag();
		
		int index = Integer.valueOf
				(lastContextMenuButton.getTag().toString());
		
		// if a the keeper's got a red card and
		// this substitution is to put another field player in the goal
		
		
		// make the number and name of the field player Black after susbs
		
		
		// Insert in General Events Table 
		ContentValues content = new ContentValues();
		content.put("Minute",
				(String) StatsFragmentTab.getTime());
		content.put("EventName", "Substitution");
		content.put(
				"PlayerName",
				ApplicationContextProvider.playerSN.get(index-1)
				+ "(No. "+ ApplicationContextProvider.playerNO.get(index-1)+")"
				+" (out) -> " 
				+ item.getTitle().toString() +" (in)");
		
		database = dbhelper.getWritableDatabase();
		database.insert("HomeTeamEvents", null, content);
		database.close();
		
		// update data structures in ApplicationContextProvider
		// the substitute player denoted by item
		// is added to the first 11
		 
		///int indexSub = ApplicationContextProvider.playerSN.indexOf(item.toString().split("No.")[1]);
		String number = item.toString().split("No.")[1];
		number = number.substring(0, number.length()-1);
		//int playerNo = Integer.valueOf(number);
		int indexSub = ApplicationContextProvider.playerNO.indexOf(number);
		
		ApplicationContextProvider.playerFN.set(index-1, ApplicationContextProvider.playerFN.get(indexSub));
		ApplicationContextProvider.playerSN.set(index-1, ApplicationContextProvider.playerSN.get(indexSub));
		ApplicationContextProvider.playerNO.set(index-1, ApplicationContextProvider.playerNO.get(indexSub));
		ApplicationContextProvider.playerRC.set(index-1, ApplicationContextProvider.playerRC.get(indexSub));
		
		ApplicationContextProvider.playerFN.remove(indexSub);
		ApplicationContextProvider.playerSN.remove(indexSub);
		ApplicationContextProvider.playerNO.remove(indexSub);
		ApplicationContextProvider.playerRC.remove(indexSub);
		
		// decrement the number of subs left in the game 
		ApplicationContextProvider.noSubsLeft --;
		
		// update the name on screen for player
		
		switch (index) 
		{
			case 1:
				player1NoText.setText(ApplicationContextProvider.playerNO.get(0));
				player1Text.setText(ApplicationContextProvider.playerSN.get(0));
			break;

			case 2:
				player2NoText.setText(ApplicationContextProvider.playerNO.get(1));
				player2Text.setText(ApplicationContextProvider.playerSN.get(1));
			break;
			case 3:
				player3NoText.setText(ApplicationContextProvider.playerNO.get(2));
				player3Text.setText(ApplicationContextProvider.playerSN.get(2));
			break;			
			case 4:
				player4NoText.setText(ApplicationContextProvider.playerNO.get(3));
				player4Text.setText(ApplicationContextProvider.playerSN.get(3));
			break;
			case 5:
				player5NoText.setText(ApplicationContextProvider.playerNO.get(4));
				player5Text.setText(ApplicationContextProvider.playerSN.get(4));
			break;
			case 6:
				player6NoText.setText(ApplicationContextProvider.playerNO.get(5));
				player6Text.setText(ApplicationContextProvider.playerSN.get(5));
			break;
			case 7:
				player7NoText.setText(ApplicationContextProvider.playerNO.get(6));
				player7Text.setText(ApplicationContextProvider.playerSN.get(6));
			break;
			case 8:
				player8NoText.setText(ApplicationContextProvider.playerNO.get(7));
				player8Text.setText(ApplicationContextProvider.playerSN.get(7));
			break;
			case 9:
				player9NoText.setText(ApplicationContextProvider.playerNO.get(8));
				player9Text.setText(ApplicationContextProvider.playerSN.get(8));
			break;
			case 10:
				player10NoText.setText(ApplicationContextProvider.playerNO.get(9));
				player10Text.setText(ApplicationContextProvider.playerSN.get(9));
			break;
			case 11:
				player11NoText.setText(ApplicationContextProvider.playerNO.get(10));
				player11Text.setText(ApplicationContextProvider.playerSN.get(10));
			break;
		default:
			break;
		}
		
		// update the number on screen for player
		
		
		return true;
	}



	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		int ID = v.getId();
		
		// if goalkeeper 
		if (ID == R.id.imageButton1) 
		{
			if (ApplicationContextProvider.playerRC.get(0).equalsIgnoreCase("1"))
			{
				for (int i = 1; i < ApplicationContextProvider.teamSize; i++)
					menu.add(0,	v.getId(),0,ApplicationContextProvider.playerSN.get(i)
									+ " (No."+ ApplicationContextProvider.playerNO.get(i)+")");
				lastContextMenuButton = v;
			}
			else 
				if (ApplicationContextProvider.noSubsLeft > 0)
				{
					for (int i = 11; i < ApplicationContextProvider.playerFN.size(); i++)
						if (ApplicationContextProvider.playerRC.get(0).equalsIgnoreCase("0"))
							menu.add(0, v.getId(), 0,
								ApplicationContextProvider.playerSN.get(i) + " (No."
										+ ApplicationContextProvider.playerNO.get(i)+")");
					lastContextMenuButton = v;
				}
				else
					Toast.makeText(
							getActivity(),
							"You've exhausted the number of substitutions allowed in this game!", Toast.LENGTH_SHORT)
							.show();
		}
		else 
		{
			// if selected player is not goalkeeper
			// and player has a Red Card 
			//do not pop up subs Context Menu
			int index = Integer.valueOf(v.getTag().toString())-1;
			if (ApplicationContextProvider.playerRC.get(index)== "0")
				if (ApplicationContextProvider.noSubsLeft > 0)
				{
					for (int i = 11; i < ApplicationContextProvider.playerFN.size(); i++)
						if (ApplicationContextProvider.playerRC.get(0).equalsIgnoreCase("0"))
							menu.add(0, v.getId(), 0,
								ApplicationContextProvider.playerSN.get(i) + " (No."
										+ ApplicationContextProvider.playerNO.get(i)+")");
					lastContextMenuButton = v;
				}
				else
					Toast.makeText(
							getActivity(),
							"You've exhausted the number of substitution in this game", Toast.LENGTH_SHORT)
							.show();
			else
				Toast.makeText(
						getActivity(),
						"No substitutions allowed for eliminated players unless they are the goalkeeper.",
						Toast.LENGTH_SHORT).show();
		}
	}
		
	}


