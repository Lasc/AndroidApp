package com.lasc.mcmullen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class LiveGameRecording extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acticity_live_game_recording);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void gotoStatsActivity(View v){
		if (ApplicationContextProvider.recordingInProgress == false)
			Toast.makeText(getApplicationContext(), "You have no recordings in progress!", Toast.LENGTH_LONG).show();
		else
		{
			ApplicationContextProvider.recordingInProgress = true;
			Intent intent = new Intent(this, StatsActivity.class);
			startActivity(intent);
		}
	}

	public void gotoStartNewRecording(View v){
		if (ApplicationContextProvider.recordingInProgress == true)
			Toast.makeText(getApplicationContext(), "You have recordings in progress! ", Toast.LENGTH_LONG).show();
		else
		{
			Intent intent = new Intent(this, StartNewRecordingActivity.class);
			getIntent().putExtra("START_TIME", 0.0);
			startActivity(intent);
		}
	}

}
