package com.lasc.mcmullen;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.MenuItem;

public class StatsActivity extends Activity {
	
    // Declaring our tabs and the corresponding fragments.

	ActionBar.Tab homeTeamTab, eventsTab, awayTeamTab,passTab ;
	    
    Fragment homeFragmentTab = new HomeTeamFragmentTab();
    Fragment eventsFragmentTab = new StatsFragmentTab();
    Fragment passFragmentTab = new PassFragmentTab();
    Fragment awayFragmentTab = new AwayTeamFragmentTab();

	 @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stats);
		
		// The 3 Tabs visible in this activity:
		// the Home Team Tab. the Stats Team TAB and the Away Team TAB
		// Asking for the default ActionBar element that our platform supports.
		ActionBar actionBar = getActionBar();
		
		// for the Back functionality 
		getActionBar().setDisplayHomeAsUpEnabled(true); 

		// Creating ActionBar tabs.
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the tabs.
		homeTeamTab = actionBar.newTab();
		eventsTab = actionBar.newTab();
		passTab = actionBar.newTab();
		awayTeamTab = actionBar.newTab();
		
		//Set tab titles
		homeTeamTab.setText("My\nTeam");
		eventsTab.setText("General\nStats");
		passTab.setText("Passes");
		awayTeamTab.setText("Other\nTeam");
		
		// Setting tab listeners.
		homeTeamTab.setTabListener(new TabListener(homeFragmentTab));
		eventsTab.setTabListener(new TabListener(eventsFragmentTab));
		passTab.setTabListener(new TabListener(passFragmentTab));
		awayTeamTab.setTabListener(new TabListener(awayFragmentTab));
		
		// Adding tabs to the ActionBar.
		actionBar.addTab(homeTeamTab);
		actionBar.addTab(eventsTab);
		actionBar.addTab(passTab);
		actionBar.addTab(awayTeamTab);
		
		
	}
	 
	 @Override
		public boolean onOptionsItemSelected(MenuItem item) {
			if (item.getItemId() == android.R.id.home) {
				finish();
			}
			return super.onOptionsItemSelected(item);
		}
}
