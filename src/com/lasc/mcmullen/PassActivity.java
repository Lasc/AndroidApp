package com.lasc.mcmullen;

import java.util.ArrayList;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TableLayout.LayoutParams;

public class PassActivity extends Activity {
	
    private SQLiteDatabase database;
    private SQLiteOpenHelper dbhelper;
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pass_activity);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		
		//Read recorded Pass Completion stats from the database 
		
		dbhelper = new HomeTeamDBHelper(this);
		database = dbhelper.getReadableDatabase();
		ArrayList<ArrayList<String>> passes = ((HomeTeamDBHelper) dbhelper).readPasses(database);

		dbhelper.close();
		
		//Build Table with ALL the pass sequences in the match
        TableLayout passTable = (TableLayout)findViewById(R.id.passStatsTable);;
        
		//headers row
        TableRow tr_head = new TableRow(passTable.getContext());
        tr_head.setId(18);
        tr_head.setBackgroundColor(Color.GRAY);
        tr_head.setLayoutParams(new LayoutParams(
        		LayoutParams.WRAP_CONTENT,
        		LayoutParams.WRAP_CONTENT));
        
		//column headers
        TextView label_passNo = new TextView(passTable.getContext());
        label_passNo.setId(20);
        label_passNo.setText("Pass\n No.");
        label_passNo.setTextColor(Color.BLACK);
        label_passNo.setPadding(5, 5, 5, 5);
        tr_head.addView(label_passNo);

        TextView label_passMin = new TextView(passTable.getContext());
        label_passMin.setId(20);
        label_passMin.setText("Minute\n No.");
        label_passMin.setTextColor(Color.BLACK);
        label_passMin.setPadding(5, 5, 5, 5);
        tr_head.addView(label_passMin);
        
        TextView label_Players = new TextView(passTable.getContext());
        label_Players.setId(21);
        label_Players.setText("Pass sequence\nbetween players"); 
        label_Players.setTextColor(Color.BLACK); 
        label_Players.setPadding(5, 5, 5, 5); 
        tr_head.addView(label_Players); 
        
        passTable.addView(tr_head, new TableLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT));
       
        passTable.setStretchAllColumns(true);

		if (passes != null)
		{
			// number of currently recorded passes in the DB
			// is equal to the number of rows in the table displaying them
			for (int i=0; i<passes.size(); i++)
			{
				// Create the table row
				TableRow tr = new TableRow(passTable.getContext());
				if(i%2!=0) tr.setBackgroundColor(Color.GRAY);
				tr.setId(100+i);
				tr.setLayoutParams(new LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));
				
				TextView labelPassNo = new TextView(passTable.getContext());
				labelPassNo.setId(200+i); 
				labelPassNo.setText(new Integer(i).toString());
				labelPassNo.setPadding(2, 0, 0, 0);
				labelPassNo.setTextColor(Color.BLACK);
				tr.addView(labelPassNo);
				
				for (int k=1; k<passes.get(i).size(); k++)
				{
					TextView labelPlayers = new TextView(passTable.getContext());
					labelPlayers.setId(200+i);
					labelPlayers.setText(passes.get(i).get(k));
					labelPlayers.setPadding(5, 0, 15, 0);
					labelPlayers.setTextColor(Color.BLACK);
					tr.addView(labelPlayers);
				}
				
				passTable.addView(tr, new TableLayout.LayoutParams(
					                    LayoutParams.WRAP_CONTENT,
					                    LayoutParams.WRAP_CONTENT));
				
			}
		}
	}
	
}
