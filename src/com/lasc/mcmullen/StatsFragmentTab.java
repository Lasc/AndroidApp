package com.lasc.mcmullen;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TooManyListenersException;

import com.opencsv.CSVWriter;

import android.app.Fragment;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;

public class StatsFragmentTab extends Fragment  {
    private SQLiteDatabase database;
    private SQLiteOpenHelper dbhelper;
    private static Chronometer chronometer;
    private Button timer, cancelGame;

    
    public static CharSequence getTime()
    {
    	if (chronometer == null)
    		return "00:00";
    	return chronometer.getText();
    }
    
	@Override
	public void onCreateOptionsMenu(
	      Menu menu, MenuInflater inflater) {
	   inflater.inflate(R.menu.action_bar_general_stats, menu);
	}
	

    @Override
	public void onResume() {
        super.onResume();
        	//new Date().getTime();
        	
        	if (timer.getText().toString().equals("Stop Timer"))
            {
        		
        		//chronometer.setBase(SystemClock.elapsedRealtime() - timeWhenStopped +currentTime);
        		//chronometer.setText("00:12");
        		chronometer.setBase(ApplicationContextProvider.TIME_SPENT_ON_LEVEL);
        		chronometer.start();
            }
    }
    
    @Override
	public void onPause() {
        super.onPause();
        if (timer.getText().toString().equals("Stop Timer"))
        {
        	//timeWhenStopped = SystemClock.elapsedRealtime() - chronometer.getBase();
        	//ApplicationContextProvider.TIME_SPENT_ON_LEVEL =  chronometer.getBase() - SystemClock.elapsedRealtime(); 
        	chronometer.stop();
        }
    }
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) 
		{
			case R.id.action_export:
				String csv = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
				String fileName = csv+"/GenStats_"+
						ApplicationContextProvider.myTeamName  +"_"
						+ ApplicationContextProvider.otherTeamName  +"_"
						+ ApplicationContextProvider.location +" "
						+ApplicationContextProvider.matchDate + ".csv";
				CSVWriter writer;
			try {
				writer = new CSVWriter(new FileWriter(fileName));
				dbhelper = new HomeTeamDBHelper(getActivity());
				database = dbhelper.getReadableDatabase();
				ArrayList<ArrayList<String>> generalEventsStats = ((HomeTeamDBHelper) dbhelper).readGeneralEvents(database);

				dbhelper.close();

				if (generalEventsStats != null)
				{ 
					if (generalEventsStats.size() > 0)
					{
						// number of currently recorded passes in the DB
						// is equal to the number of rows in the table displaying them
						List<String[]> data = new ArrayList<String[]>();
	
						for (int i=0; i<generalEventsStats.size(); i++)
						{
							String[] array = new String[generalEventsStats.get(i).size()];
							for (int k=0; k<generalEventsStats.get(i).size(); k++)
							{	
								array[k] = generalEventsStats.get(i).get(k);
							}
							data.add(array);
						}
						writer.writeAll( data);	
						writer.close();
						Toast.makeText(getActivity(), "Exported General Stats to file: "+fileName, Toast.LENGTH_LONG).show();
					}
					else
						Toast.makeText(getActivity(), "The Application did not find any entires for General Stats in database!", Toast.LENGTH_LONG).show();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		
				
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{
		View rootView = inflater.inflate(R.layout.stats_tab_layout, container, false);

		setHasOptionsMenu(true);
		chronometer = (Chronometer) rootView.findViewById(R.id.chronometer1);
		if (ApplicationContextProvider.TIME_SPENT_ON_LEVEL>0)
		{
			chronometer.setBase(ApplicationContextProvider.TIME_SPENT_ON_LEVEL);
			chronometer.start();
		}
		
		timer = (Button) rootView.findViewById(R.id.toggleButton1);
		
		timer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (timer.getText().toString().equals("Start Timer"))
				{
					chronometer.setBase(ApplicationContextProvider.TIME_SPENT_ON_LEVEL);
					chronometer.stop();
					//ApplicationContextProvider.TIME_SPENT_ON_LEVEL = chronometer.getBase() - SystemClock.elapsedRealtime();
				}
	            else
				{
	            	//chronometer.setBase(SystemClock.elapsedRealtime() + ApplicationContextProvider.TIME_SPENT_ON_LEVEL );
					chronometer.start();
					ApplicationContextProvider.TIME_SPENT_ON_LEVEL = chronometer.getBase();
					//timerStarted = chronometer.getBase();

				}
				

			}
		});
		
		cancelGame = (Button) rootView.findViewById(R.id.foulButton);
		cancelGame.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ApplicationContextProvider.recordingInProgress = false;
				ApplicationContextProvider.saveGameStateOnAppShutdown();
				Toast.makeText(getActivity(), "You have canceled the reconding. This game's data is now available from the recodings archive.", Toast.LENGTH_LONG).show();
				getActivity().finish();
				
			}
		});
		
		//Read recorded Pass Completion stats from the database 
		
		dbhelper = new HomeTeamDBHelper(getActivity());
		database = dbhelper.getReadableDatabase();
		ArrayList<ArrayList<String>> generalEventsStats = ((HomeTeamDBHelper) dbhelper).readGeneralEvents(database);

		dbhelper.close();

		//Build the General Events Table
        TableLayout eventsTable = (TableLayout)rootView.findViewById(R.id.eventsStatsTable);
        
		//headers row
        TableRow tr_head_stats = new TableRow(eventsTable.getContext());
        tr_head_stats.setId(18);
        tr_head_stats.setBackgroundColor(Color.GRAY);
        tr_head_stats.setLayoutParams(new LayoutParams(
        		LayoutParams.WRAP_CONTENT,
        		LayoutParams.WRAP_CONTENT));
        
		//column headers
        TextView minute = new TextView(eventsTable.getContext());
        minute.setId(20);
        minute.setText("Minute");
        minute.setTextColor(Color.BLACK);
        minute.setPadding(5, 5, 5, 5);
        tr_head_stats.addView(minute);// add the column to the table row here

        TextView event = new TextView(eventsTable.getContext());
        event.setId(21);// define id that must be unique
        event.setText("Event"); // set the text for the header 
        event.setTextColor(Color.BLACK); // set the color
        event.setPadding(5, 5, 5, 5); // set the padding (if required)
        tr_head_stats.addView(event); // add the column to the table row here
        
        TextView player = new TextView(eventsTable.getContext());
        player.setId(21);// define id that must be unique
        player.setText("Player"); // set the text for the header 
        player.setTextColor(Color.BLACK); // set the color
        player.setPadding(5, 5, 5, 5); // set the padding (if required)
        tr_head_stats.addView(player); // add the column to the table row here	

        
        eventsTable.addView(tr_head_stats, new TableLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT));
       
        eventsTable.setStretchAllColumns(true);

		if (generalEventsStats != null)
		{
			// number of currently recorded passes in the DB
			// is equal to the number of rows in the table displaying them
			for (int i=0; i<generalEventsStats.size(); i++)
			{
				// Create the table row
				TableRow tr = new TableRow(eventsTable.getContext());
				if(i%2!=0) tr.setBackgroundColor(Color.GRAY);
				tr.setId(100+i);
				tr.setLayoutParams(new LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));
				
				TextView labelPassNo = new TextView(eventsTable.getContext());
				labelPassNo.setId(200+i); 
				labelPassNo.setText(generalEventsStats.get(i).get(0));
				labelPassNo.setPadding(2, 0, 0, 0);
				labelPassNo.setTextColor(Color.BLACK);
				tr.addView(labelPassNo);
				
				for (int k=1; k<generalEventsStats.get(i).size(); k++)
				{
					TextView labelPlayers = new TextView(eventsTable.getContext());
					labelPlayers.setId(200+i);
					labelPlayers.setText(generalEventsStats.get(i).get(k));
					labelPlayers.setPadding(5, 0, 15, 0);
					labelPlayers.setTextColor(Color.BLACK);
					tr.addView(labelPlayers);
				}
				
				eventsTable.addView(tr, new TableLayout.LayoutParams(
					                    LayoutParams.WRAP_CONTENT,
					                    LayoutParams.WRAP_CONTENT));
				
			}
		}

		return rootView;
	}
}
