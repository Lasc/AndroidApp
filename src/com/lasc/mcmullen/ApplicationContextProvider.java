package com.lasc.mcmullen;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ApplicationContextProvider extends Application {

    /**
     * Keeps a reference of the application context
     */
	static SQLiteOpenHelper dbhelper;
	static SQLiteDatabase database;
	
	private static Context sContext;
    public static long TIME_SPENT_ON_LEVEL;
    
    public static String otherTeamName;
    public static String myTeamName;
    public static String location;
    public static String matchDate;
    public static String matchStatus;
    public static int teamSize;
    public static int noSubsLeft;

    public static ArrayList<ArrayList<String>> myTeamData;
    public static ArrayList<ArrayList<String>> otherTeamData;
    
    public static boolean recordingInProgress;
	
    public static ArrayList<String> playerFN , playerSN, playerNO, playerRC, playerYC;
    public static ArrayList<String> oplayerFN , oplayerSN, oplayerNO;


    public static void setTeams()
    {
    	// insert in Match Table
    	insertMatch();
    	
    	// populate transient content 
    	// in MyTeam in ApplicationContextProvider
    	setMyTeamVariables();
    	
    	// populate transient content 
    	// in OtherTeam in ApplicationContextProvider
    	setOtherTeamVariables();
    	
    	// insert MyTeam in HomeTeamTable
    	insertMyTeam();
    	
    	// insert OtherTeam in AwayTeamTable
    	insertOtherTeam();
    }
    
    private static void insertMyTeam() 
    {
    	dbhelper = new HomeTeamDBHelper(getContext());
    	database =  dbhelper.getWritableDatabase();
    	
		ContentValues content = new ContentValues();
    	for (int i=0; i<myTeamData.size(); i++)
    	{
    		content.put("PlayerName" , myTeamData.get(i).get(0));
    		content.put("PlayerNumber" , myTeamData.get(i).get(1));
    		content.put("PlayerPosition" , myTeamData.get(i).get(2));
    	}
		database.insert("HomeTeamTable", null, content);
		database.close();
	}

    private static void insertOtherTeam() 
    {
    	dbhelper = new HomeTeamDBHelper(getContext());
    	database =  dbhelper.getWritableDatabase();
    	
		ContentValues content = new ContentValues();
    	for (int i=0; i<otherTeamData.size(); i++)
    	{
    		content.put("PlayerName" , otherTeamData.get(i).get(0));
    		content.put("PlayerNumber" , otherTeamData.get(i).get(1));
    		content.put("PlayerPosition" , otherTeamData.get(i).get(2));
    	}
		database.insert("OtherTeam", null, content);
		database.close();
	}

    private static void insertMatch() {
    	dbhelper = new HomeTeamDBHelper(getContext());
    	database =  dbhelper.getWritableDatabase();
    	
		ContentValues content = new ContentValues();
		content.put("HomeTeam" , myTeamName);
		content.put("AwayTeam" , otherTeamName);
		content.put("Date" , matchDate);
		content.put("Status" , matchStatus);
		content.put("Location" , location);
		content.put("Minute" , "00:00");
		database.insert("MatchTable", null, content);
		database.close();		
	}
	
	private static void setOtherTeamVariables() 
    {
		//name, number, positiion
		for (int i=0; i<otherTeamData.size();i++)
		{
			oplayerFN.add(otherTeamData.get(i).get(0).split(" ")[0]);
			oplayerSN.add(otherTeamData.get(i).get(0).split(" ")[1]);
			oplayerNO.add(otherTeamData.get(i).get(1));
		}
    }

	private static void setMyTeamVariables() 
    {
		//name, number, positiion
		for (int i=0; i<myTeamData.size();i++)
		{
			playerFN.add(myTeamData.get(i).get(0).split(" ")[0]);
			playerSN.add(myTeamData.get(i).get(0).split(" ")[1]);
			playerNO.add(myTeamData.get(i).get(1));
			playerRC.add("0");
			playerYC.add("0");
		}
		
		teamSize = 11;
    	noSubsLeft = 6;
	}

	public static void saveGameStateOnAppShutdown()
    {
    	
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        
        playerFN = new ArrayList<>();
        playerSN = new ArrayList<>();
        playerNO = new ArrayList<>();
        playerRC = new ArrayList<>();
        playerYC = new ArrayList<>();
        myTeamData = new ArrayList<>();
        otherTeamData = new ArrayList<>();
        
        oplayerFN = new ArrayList<>();
        oplayerSN = new ArrayList<>();
        oplayerNO = new ArrayList<>();
        // Read the Match Table to see if there is any
        // game paused
        
        loadDefaults();
    }

    private static void loadDefaults() 
    {
    	recordingInProgress = true;
    	myTeamName = "AthloneFC";
    	otherTeamName= "DublinFC";
    	location = "Athlone";
    	DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
    	Date date = new Date();
    	matchDate = dateFormat.format(date);
    			
    	teamSize = 11;
    	noSubsLeft = 6;
    	for (int i = 0; i<25; i ++)
    		playerRC.add(i, "0");
    	
    	for (int i = 0; i<25; i ++)
    		playerYC.add(i, "0");
    	
    	playerFN.add(0, "Derek");
    	playerSN.add(0, "Doyle");
    	playerNO.add(0, "1");
    	
    	playerFN.add(1, "Ian");
    	playerSN.add(1, "Sweeney");
    	playerNO.add(1, "2");
    	
    	playerFN.add(2, "Brian");
    	playerSN.add(2, "Shortall");
    	playerNO.add(2, "3");
    	
    	playerFN.add(3, "Graham");
    	playerSN.add(3, "Rusk");
    	playerNO.add(3, "5");
    	
    	playerFN.add(4, "Philip");
    	playerSN.add(4, "Gorman");
    	playerNO.add(4, "4");

    	playerFN.add(5, "Declan");
    	playerSN.add(5, "Brennan");
    	playerNO.add(5, "14");

    	playerFN.add(6, "Sam");
    	playerSN.add(6, "O'Connor");
    	playerNO.add(6, "10");

    	playerFN.add(7, "Stephen");
    	playerSN.add(7, "Quigley");
    	playerNO.add(7, "7");

    	playerFN.add(8, "Aidan");
    	playerSN.add(8, "Collins");
    	playerNO.add(8, "9");

    	playerFN.add(9, "Barry");
    	playerSN.add(9, "Clancy");
    	playerNO.add(9, "11");

    	playerFN.add(10, "Dean");
    	playerSN.add(10, "Ebbe");
    	playerNO.add(10, "19");

    	playerFN.add(11, "Thomas");
    	playerSN.add(11, "Bryn");
    	playerNO.add(11, "12");

    	playerFN.add(12, "Eric");
    	playerSN.add(12, "Foley");
    	playerNO.add(12, "15");

    	playerFN.add(13, "Graham");
    	playerSN.add(13, "Rusk");
    	playerNO.add(13 , "23");
    	
    	playerFN.add(14, "Kyle");
    	playerSN.add(14, "Bekker");
    	playerNO.add(14, "16");

    	playerFN.add(15, "Victor");
    	playerSN.add(15, "Ulloa");
    	playerNO.add(15, "28");

    	playerFN.add(16, "Zach");
    	playerSN.add(16, "Loyd");
    	playerNO.add(16, "30");

    	playerFN.add(17, "Chris");
    	playerSN.add(17, "Seitz");
    	playerNO.add(17, "33");

    	playerFN.add(18, "Michael");
    	playerSN.add(18, "Barrios");
    	playerNO.add(18, "21");

    	playerFN.add(19, "Stephen");
    	playerSN.add(19, "Keel");
    	playerNO.add(19, "36");

    	playerFN.add(20, "Walker");
    	playerSN.add(20, "Zimmerman");
    	playerNO.add(20, "24");

    	playerFN.add(21, "Brent");
    	playerSN.add(21, "Erwin");
    	playerNO.add(21, "8");

    	playerFN.add(22, "Mike");
    	playerSN.add(22, "Jeffries");
    	playerNO.add(22, "42");

    	playerFN.add(23, "Colin");
    	playerSN.add(23, "Clarke");
    	playerNO.add(23, "39");
    	
    	playerFN.add(24, "Steve");
    	playerSN.add(24, "Morrow");
    	playerNO.add(24, "46");    	

    	
    	//   ######### Away Team
    	
    	oplayerFN.add(0, "Lee");
    	oplayerSN.add(0, "Atherton");
    	oplayerNO.add(0, "33");
    	
    	oplayerFN.add(1, "Paul");
    	oplayerSN.add(1, "Alexander");
    	oplayerNO.add(1, "26");
    	
    	oplayerFN.add(2, "Matt");
    	oplayerSN.add(2, "Lawlor");
    	oplayerNO.add(2, "23");
    	
    	oplayerFN.add(3, "George");
    	oplayerSN.add(3, "Bowyer");
    	oplayerNO.add(3, "19");
    	
    	oplayerFN.add(4, "Zach");
    	oplayerSN.add(4, "Clarke");
    	oplayerNO.add(4, "5");

    	oplayerFN.add(5, "Chris");
    	oplayerSN.add(5, "Marlow");
    	oplayerNO.add(5, "83");

    	oplayerFN.add(6, "Mark");
    	oplayerSN.add(6, "Brown");
    	oplayerNO.add(6, "78");

    	oplayerFN.add(7, "Mick");
    	oplayerSN.add(7, "Taylor");
    	oplayerNO.add(7, "10");

    	oplayerFN.add(8, "Stuart");
    	oplayerSN.add(8, "Barton");
    	oplayerNO.add(8, "25");

    	oplayerFN.add(9, "Gus");
    	oplayerSN.add(9, "Wright");
    	oplayerNO.add(9, "8");

    	oplayerFN.add(10, "Michael");
    	oplayerSN.add(10, "Kay");
    	oplayerNO.add(10, "9");

    	oplayerFN.add(11, "Matthew");
    	oplayerSN.add(11, "Thompson");
    	oplayerNO.add(11, "88");

    	oplayerFN.add(12, "Kieran");
    	oplayerSN.add(12, "Charnock");
    	oplayerNO.add(12, "15");

    	oplayerFN.add(13, "Craig");
    	oplayerSN.add(13, "Mahon");
    	oplayerNO.add(13 , "24");
    	
    	oplayerFN.add(14, "John");
    	oplayerSN.add(14, "Rooney");
    	oplayerNO.add(14, "17");

    	oplayerFN.add(15, "Peter");
    	oplayerSN.add(15, "Winn");
    	oplayerNO.add(15, "29");

    	oplayerFN.add(16, "Jamie");
    	oplayerSN.add(16, "Menagh");
    	oplayerNO.add(16, "31");

    	oplayerFN.add(17, "Tom");
    	oplayerSN.add(17, "Peers");
    	oplayerNO.add(17, "34");

    	oplayerFN.add(18, "Ben");
    	oplayerSN.add(18, "Greenop");
    	oplayerNO.add(18, "22");

    	oplayerFN.add(19, "Matty");
    	oplayerSN.add(19, "McGinn");
    	oplayerNO.add(19, "37");

    	oplayerFN.add(20, "Lewis");
    	oplayerSN.add(20, "Turner");
    	oplayerNO.add(20, "27");

    	oplayerFN.add(21, "Paul");
    	oplayerSN.add(21, "Futcher");
    	oplayerNO.add(21, "81");

    	oplayerFN.add(22, "Gerry");
    	oplayerSN.add(22, "Keenan");
    	oplayerNO.add(22, "43");

    	oplayerFN.add(23, "Paul");
    	oplayerSN.add(23, "Roderick");
    	oplayerNO.add(23, "40");
    	
    	oplayerFN.add(24, "Micky");
    	oplayerSN.add(24, "Burns");
    	oplayerNO.add(24, "47"); 
	}
    /**
     * Returns the application context
     *
     * @return application context
     */
    public static Context getContext() {
        return sContext;
    }

    
    
}
