package com.lasc.mcmullen;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.opencsv.CSVWriter;

import android.app.Fragment;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TableLayout.LayoutParams;

public class PassFragmentTab extends Fragment {
    private SQLiteDatabase database;
    private SQLiteOpenHelper dbhelper;
    private Button passActivity;
    
	@Override
	public void onCreateOptionsMenu(
	      Menu menu, MenuInflater inflater) {
	   inflater.inflate(R.menu.action_bar_pass_stats, menu);
	}
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) 
		{
			case R.id.action_export:
				String csv = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
				String fileName = csv+"/PassStats_"+
						ApplicationContextProvider.myTeamName  +"_"
						+ ApplicationContextProvider.otherTeamName  +"_"
						+ ApplicationContextProvider.location +" "
						+ApplicationContextProvider.matchDate + ".csv";
				CSVWriter writer;
			try {
				writer = new CSVWriter(new FileWriter(fileName));
				dbhelper = new HomeTeamDBHelper(getActivity());
				database = dbhelper.getReadableDatabase();
				Map<String, ArrayList<ArrayList<String>>> passStats = ((HomeTeamDBHelper) dbhelper).readPassStats(database);
				ArrayList<ArrayList<String>> allPasses = ((HomeTeamDBHelper) dbhelper).readPasses(database);

				dbhelper.close();
				
				HashMap <String,Integer> freq = new HashMap<>();;
				if (passStats.get("Lost Possession").size()!=0)
				{
					ArrayList<ArrayList<String>> passes = passStats.get("Lost Possession");
					for ( ArrayList<String> it : passes )
					{
						Integer value = freq.put(it.get(it.size()-2), 1);
						if (value!=null)
							freq.put(it.get(it.size()-2), value + 1);
					}
					
					freq = sortByValues(freq);
				}
				
				Set <String> keys = freq.keySet();

				if (passStats != null)
				{ 
					if (passStats.size() > 0)
					{

						List<String[]> data = new ArrayList<String[]>();
						String[] arrayHelper = new String[6];
						arrayHelper[0] = "Pass Statistics\n";
						arrayHelper[1] = "Number of passes ending in Out: " + passStats.get("Out").size()+"\n";
						arrayHelper[2] = "Number of passes ending in Corner: " + passStats.get("Corner").size()+"\n";
						arrayHelper[3] = "Number of Goal Assists: " + passStats.get("Goal").size()+"\n";
						arrayHelper[4] = "Number of passes ending in Offside: " + passStats.get("Offside").size()+"\n";
						arrayHelper[5] = "Number of passes ending in Loss of Possession: " + passStats.get("Lost Possession").size()+"\n";

						for (int i=0; i<6; i++)
						{
							String[] array = new String[1];
							array[0] = arrayHelper[i];
							data.add(array);
						}
						String [] secTable = new String [1];
						secTable[0] = "Possession Loss Statistics";
						data.add(secTable);
						
						for (String name : keys)
						{
							String[] array = new String[1];
							array[0] = name + " lost the ball " +freq.get(name)+" times.";
							data.add(array);
						}
						String [] thirdTable = new String [1];
						thirdTable[0] = "All Pass Sequences in This Match";
						data.add(thirdTable);
						
						if (allPasses != null)
						{
							for (int i=0; i<allPasses.size(); i++)
							{
								String[] array = new String[allPasses.get(i).size()];
								for (int k=0; k<allPasses.get(i).size(); k++)
									array[k] = allPasses.get(i).get(k);
								data.add(array);

							}
						}
						writer.writeAll( data);	
						writer.close();
						Toast.makeText(getActivity(), "Exported Pass Stats to file: "+fileName, Toast.LENGTH_LONG).show();
					}
					else
						Toast.makeText(getActivity(), "The Application did not find any entires for Pass Stats in database!", Toast.LENGTH_LONG).show();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}

				
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{
		View rootView = inflater.inflate(R.layout.pass_tab_layout, container, false);
		setHasOptionsMenu(true);

//		chronometer = (Chronometer) rootView.findViewById(R.id.chronometer1);
//		timer = (Button) rootView.findViewById(R.id.toggleButton1);
//		timer.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				if (timer.getText().toString().equals("Start Timer"))
//					chronometer.stop();
//				else
//				{
//					chronometer.start();
//				}
//			}
//		});
//		
//		cancelGame = (Button) rootView.findViewById(R.id.foulButton);
//		cancelGame.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				ApplicationContextProvider.recordingInProgress = false;
//				Toast.makeText(getActivity(), "You have canceled the reconding. This game's data is now available from the recodings archive.", Toast.LENGTH_LONG).show();
//				getActivity().finish();
//				
//			}
//		});
		
		//Read recorded Pass Completion stats from the database 
		
		dbhelper = new HomeTeamDBHelper(getActivity());
		database = dbhelper.getReadableDatabase();
		Map<String, ArrayList<ArrayList<String>>> passStats = ((HomeTeamDBHelper) dbhelper).readPassStats(database);
		dbhelper.close();

		
		//Build the Pass Completion Stats Table
        TableLayout passStatsTable = (TableLayout)rootView.findViewById(R.id.passStatsTable);
       
        passStatsTable.setStretchAllColumns(true);

		if (passStats != null)
		{
			TableRow trOuts = new TableRow(passStatsTable.getContext());
			trOuts.setLayoutParams(new LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));
			TextView labelPassNo = new TextView(passStatsTable.getContext());
			labelPassNo.setText("Number of passes ending in Out");
			labelPassNo.setPadding(2, 0, 0, 0);
			labelPassNo.setTextColor(Color.BLACK);
			trOuts.addView(labelPassNo);
				
			TextView labelPlayers = new TextView(passStatsTable.getContext());
			labelPlayers.setText(new Integer(passStats.get("Out").size()).toString());
			labelPlayers.setPadding(5, 0, 15, 0);
			labelPlayers.setTextColor(Color.BLACK);
			trOuts.addView(labelPlayers);
			passStatsTable.addView(trOuts, new TableLayout.LayoutParams(
					                    LayoutParams.WRAP_CONTENT,
					                    LayoutParams.WRAP_CONTENT));
				
			
			TableRow trCorner = new TableRow(passStatsTable.getContext());
			trCorner.setBackgroundColor(Color.GRAY);
			trCorner.setLayoutParams(new LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));
				
			TextView labelCorner = new TextView(passStatsTable.getContext());
			labelCorner.setText("Number of passes ending in Corner");
			labelCorner.setPadding(2, 0, 0, 0);
			labelCorner.setTextColor(Color.BLACK);
			trCorner.addView(labelCorner);
				
			TextView labelNoCorner = new TextView(passStatsTable.getContext());
			labelNoCorner.setText(new Integer (passStats.get("Corner").size()).toString());
			labelNoCorner.setPadding(5, 0, 15, 0);
			labelNoCorner.setTextColor(Color.BLACK);
			trCorner.addView(labelNoCorner);
			passStatsTable.addView(trCorner, new TableLayout.LayoutParams(
					                    LayoutParams.WRAP_CONTENT,
					                    LayoutParams.WRAP_CONTENT));
			
			
			TableRow trPossession = new TableRow(passStatsTable.getContext());
			trPossession.setLayoutParams(new LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));
				
			TextView labelPossession = new TextView(passStatsTable.getContext());
			labelPossession.setText("Number of passes ending in Possession Loss  ");
			labelPossession.setPadding(2, 0, 0, 0);
			labelPossession.setTextColor(Color.BLACK);
			trPossession.addView(labelPossession);
				
			TextView labelNoPoss = new TextView(passStatsTable.getContext());
			labelNoPoss.setText(new Integer (passStats.get("Lost Possession").size()).toString());
			labelNoPoss.setPadding(5, 0, 15, 0);
			labelNoPoss.setTextColor(Color.BLACK);
			trPossession.addView(labelNoPoss);
			passStatsTable.addView(trPossession, new TableLayout.LayoutParams(
					                    LayoutParams.WRAP_CONTENT,
					                    LayoutParams.WRAP_CONTENT));
			
			
			TableRow trGoal = new TableRow(passStatsTable.getContext());
			trGoal.setBackgroundColor(Color.GRAY);
			trGoal.setLayoutParams(new LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));
				
			TextView labelGoal = new TextView(passStatsTable.getContext());
			labelGoal.setText("Number of Goal Assists");
			labelGoal.setPadding(2, 0, 0, 0);
			labelGoal.setTextColor(Color.BLACK);
			trGoal.addView(labelGoal);
				
			TextView labelNoGoal = new TextView(passStatsTable.getContext());
			labelNoGoal.setText(new Integer (passStats.get("Goal").size()).toString());
			labelNoGoal.setPadding(5, 0, 15, 0);
			labelNoGoal.setTextColor(Color.BLACK);
			trGoal.addView(labelNoGoal);
			passStatsTable.addView(trGoal, new TableLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT));
			
			
			TableRow trOffside = new TableRow(passStatsTable.getContext());
			trOffside.setLayoutParams(new LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));
				
			TextView labelOffside = new TextView(passStatsTable.getContext());
			labelOffside.setText("Number of passes ending in Offside");
			labelOffside.setPadding(2, 0, 0, 0);
			labelOffside.setTextColor(Color.BLACK);
			trOffside.addView(labelOffside);
				
			TextView labelNoOffside = new TextView(passStatsTable.getContext());
			labelNoOffside.setText(new Integer (passStats.get("Offside").size()).toString());
			labelNoOffside.setPadding(5, 0, 15, 0);
			labelNoOffside.setTextColor(Color.BLACK);
			trOffside.addView(labelNoOffside);
			passStatsTable.addView(trOffside, new TableLayout.LayoutParams(
					                    LayoutParams.WRAP_CONTENT,
					                    LayoutParams.WRAP_CONTENT));			

			HashMap <String,Integer> freq = new HashMap<>();;
			if (passStats.get("Lost Possession").size()!=0)
			{
				ArrayList<ArrayList<String>> passes = passStats.get("Lost Possession");
				for ( ArrayList<String> it : passes )
				{
					Integer value = freq.put(it.get(it.size()-2), 1);
					if (value!=null)
						freq.put(it.get(it.size()-2), value + 1);
				}
				
				freq = sortByValues(freq);
			}
			
			Set <String> keys = freq.keySet();
			
			//Build the Possession Loss Stats Table
	        TableLayout possessionLossTable = (TableLayout)rootView.findViewById(R.id.possesssionStatsTable);
	        passStatsTable.setStretchAllColumns(true);
			//headers row
	        TableRow tr_head = new TableRow(possessionLossTable.getContext());
	        tr_head.setId(18);
	        tr_head.setBackgroundColor(Color.GRAY);
	        tr_head.setLayoutParams(new LayoutParams(
	        		LayoutParams.WRAP_CONTENT,
	        		LayoutParams.WRAP_CONTENT));
	        
			//column headers
	        TextView minute = new TextView(possessionLossTable.getContext());
	        minute.setId(20);
	        minute.setText("Player Name");
	        minute.setTextColor(Color.BLACK);
	        minute.setPadding(5, 5, 5, 5);
	        tr_head.addView(minute);// add the column to the table row here

	        TextView event = new TextView(possessionLossTable.getContext());
	        event.setId(21);// define id that must be unique
	        event.setText("No. of times\nplayer lost the ball"); // set the text for the header 
	        event.setTextColor(Color.BLACK); // set the color
	        event.setPadding(35, 5, 5, 5); // set the padding (if required)
	        tr_head.addView(event); // add the column to the table row here
	        
			possessionLossTable.addView(tr_head, new TableLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT));
			
	        int i=0;
			for (String name : keys)
			{
				// Create the table row
				TableRow tr = new TableRow(possessionLossTable.getContext());
				if(i%2!=0) tr.setBackgroundColor(Color.GRAY);
				tr.setId(100+i);
				tr.setLayoutParams(new LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));
				
				TextView playerName = new TextView(possessionLossTable.getContext());
				playerName.setId(200+i); 
				playerName.setText(name);
				playerName.setPadding(2, 0, 0, 0);
				playerName.setTextColor(Color.BLACK);
				tr.addView(playerName);
				
				TextView noOfTimes = new TextView(possessionLossTable.getContext());
				noOfTimes.setId(200+i);
				noOfTimes.setText(new Integer (freq.get(name)).toString());
				noOfTimes.setPadding(0, 0, 0, 0);
				noOfTimes.setTextColor(Color.BLACK);
				noOfTimes.setGravity(Gravity.CENTER);
				tr.addView(noOfTimes);
				
				possessionLossTable.addView(tr, new TableLayout.LayoutParams(
	                    LayoutParams.WRAP_CONTENT,
	                    LayoutParams.WRAP_CONTENT));
		        i++;
			}


		}
		
		passActivity = (Button) rootView.findViewById(R.id.button1);
		passActivity.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), PassActivity.class);
				startActivity(intent);
			}
		});				

		return rootView;

	}
    
    private static HashMap sortByValues(HashMap map) { 
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
             public int compare(Object o1, Object o2) {
                return ((Comparable)((Map.Entry) (o2)).getValue()).compareTo(((Map.Entry) (o1)).getValue() );
             }
        });

        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
               Map.Entry entry = (Map.Entry) it.next();
               sortedHashMap.put(entry.getKey(), entry.getValue());
        } 
        return sortedHashMap;
   }
}
