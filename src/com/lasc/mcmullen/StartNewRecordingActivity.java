package com.lasc.mcmullen;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TableLayout.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class StartNewRecordingActivity extends Activity 
			implements OnItemSelectedListener {
	private int mYear;
	private int mMonth;
	private int mDay;
	private Button addHomeTeamName, matchDate, addPlayer;
	private EditText matchDateEditText, teamNameEditText, playerNameEditText,
			playerNumberEditText;
	static final int DATE_DIALOG_ID = 0;
	boolean teamNameSet;
	private String teamName;
	
	private ArrayList<ArrayList<String>> myTeamData;
	
	private TableLayout table; 

	SQLiteDatabase database;
	SQLiteOpenHelper dbhelper;
	private static int playerNo = 0;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.action_bar_for_add_new_team, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.action_save:
			if (playerNo<3)
				Toast.makeText(getApplicationContext(), 
						"You must eneter 25 players. " +
						"Your team only cointains "+playerNo+ " players.",
					Toast.LENGTH_LONG).show();
			else 
				if (teamNameSet==false)
					Toast.makeText(getApplicationContext(), 
							"The Home Team's name was not entered. ",
							Toast.LENGTH_SHORT).show();
				else
				{
					new AlertDialog.Builder(this)
			        .setIcon(android.R.drawable.ic_dialog_alert)
			        .setTitle("Add Home Team")
			        .setMessage("Are sure this is the team structure you wish to add?")
			        .setPositiveButton("YES", new DialogInterface.OnClickListener() {

			            @Override
			            public void onClick(DialogInterface dialog, int which) {
							 // INSERT Home Team IN 
			            	 // ApplicationContextProvider HERE
			            	 ApplicationContextProvider.myTeamData = myTeamData;
			            	 ApplicationContextProvider.myTeamName = teamName;
			            	 ApplicationContextProvider.matchDate = matchDateEditText.getText().toString();
			            	 ApplicationContextProvider.matchStatus = "Paused";
			            	 ApplicationContextProvider.recordingInProgress = true;
			            			 
			                //Stop the activity
			            	StartNewRecordingActivity.this.finish();  
			            	
			            	// transition to 
			            	// the add away team activty:
			            	
			        		Intent intent = new Intent(getApplicationContext(), AddAwayTeamActivity.class);
			        		startActivity(intent);
			            }

			        })
			        .setNegativeButton("NO", null)
			        .show();
					
					
				}
			return true;
			
		case R.id.action_delete_one_player:

			AlertDialog.Builder b = new Builder(this);
			    b.setTitle("Choose player number to delete");
			    String[] items = new String[table.getChildCount()-2];
			    int k=0;
				for (int i = 2; i < table.getChildCount(); i++) 
				{
					View parentRow = table.getChildAt(i);
					if(parentRow instanceof TableRow)
					{
						TextView button = (TextView ) ((ViewGroup) parentRow).getChildAt(0);
			            if(button instanceof TextView)
			            {
			            	items[k] = button.getText().toString();
			            	k++;
			            }
					}
				}

			    b.setItems(items, new OnClickListener() {

			        @Override
			        public void onClick(DialogInterface dialog, int which) {

			            dialog.dismiss();
			            	Toast.makeText(getApplicationContext(), 
			    					"Player "+which,
			    					Toast.LENGTH_SHORT).show();
			            	
			            	table.removeViewAt(which+2);
			            }
			    });

			    b.show();
			    
			return true;
			
		case R.id.action_delete_entire_team:

				table.removeViews(2, table.getChildCount()-2);

		return true;
		
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.start_new_rec);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		myTeamData = new ArrayList<>();
		playerNo = 0;
				
		Spinner spinner1 = (Spinner) findViewById(R.id.player_position);
		spinner1.setOnItemSelectedListener(this);
		ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(
				this, R.array.player_pos_array,
				android.R.layout.simple_spinner_item);

		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner1.setAdapter(adapter1);

		table = (TableLayout) this
				.findViewById(R.id.homeTeamTable);
		
		//title row
		TableRow rowTitle = new TableRow(table.getContext());
		rowTitle.setId(10);
		rowTitle.setBackgroundColor(Color.GRAY);
		rowTitle.setGravity(Gravity.CENTER_HORIZONTAL);

		// title cell
		final TextView title = new TextView(this);
		title.setText("Home Team");
		title.setId(20);
		title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
		title.setGravity(Gravity.CENTER);
		title.setTypeface(Typeface.SERIF, Typeface.BOLD);
		rowTitle.addView(title);
		table.addView(rowTitle);

		TableRow columns_header = new TableRow(table.getContext());
		columns_header.setId(30);
		columns_header.setBackgroundColor(Color.GRAY);
		columns_header.setLayoutParams(new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		TextView playerNumber = new TextView(table.getContext());
		playerNumber.setId(60);
		playerNumber.setText("Player\nNumber");
		playerNumber.setTextColor(Color.BLACK);
		playerNumber.setPadding(5, 5, 0, 5);
		columns_header.addView(playerNumber);
		
		TextView position = new TextView(table.getContext());
		position.setId(40);
		position.setText("Player\nPosition");
		position.setTextColor(Color.BLACK);
		position.setPadding(5, 5, 5, 5);
		columns_header.addView(position);

		TextView playerName = new TextView(table.getContext());
		playerName.setId(50);
		playerName.setText("Player\nName");
		playerName.setTextColor(Color.BLACK);
		playerName.setPadding(5, 5, 5, 5);
		columns_header.addView(playerName);

		table.addView(columns_header, new TableLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		table.setStretchAllColumns(true);

		addHomeTeamName = (Button) findViewById(R.id.button2);
		teamNameEditText = (EditText) findViewById(R.id.EditText01);

		addHomeTeamName.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (teamNameEditText.getText().toString().equalsIgnoreCase("")) {
					Toast.makeText(getApplicationContext(),
							"The Team's name was not typed.",
							Toast.LENGTH_SHORT).show();
				} else {
					teamNameSet = true;

					teamName = teamNameEditText.getText().toString();
					// See if there's already a recording
					// in progress Home Team
					// Check by Date and Team names

					// if no record in progress

					title.setText(teamName);

					// hide the keyboard
					InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					mgr.hideSoftInputFromWindow(
							teamNameEditText.getWindowToken(), 0);

				}
			}
		});

		playerNameEditText = (EditText) findViewById(R.id.textView2);
		playerNumberEditText = (EditText) findViewById(R.id.textView3);

		addPlayer = (Button) findViewById(R.id.foulButton);
		addPlayer.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				mgr.hideSoftInputFromWindow(addPlayer.getWindowToken(), 0);

				String player_position_text = ((Spinner) findViewById(R.id.player_position))
						.getSelectedItem().toString();
				if (player_position_text
						.equalsIgnoreCase("Choose Player Position")) {
					Toast.makeText(getApplicationContext(),
							"The Player's position was not chosen.",
							Toast.LENGTH_SHORT).show();
				} else if (playerNameEditText.getText().toString()
						.equalsIgnoreCase("")) {
					Toast.makeText(getApplicationContext(),
							"The Player's name was not typed.",
							Toast.LENGTH_SHORT).show();
				} else if (playerNumberEditText.getText().toString()
						.equalsIgnoreCase("")) {
					Toast.makeText(getApplicationContext(),
							"The Player's number was not typed.",
							Toast.LENGTH_SHORT).show();
				} else {
					// check if a player with this number 
					// has been already entered in the table
					String playerNumber = playerNumberEditText.getText()
							.toString();
					if (table.getChildCount()>2)
					{
						// get players' numbers from the exiting table on screen
						String text[] = new String[table.getChildCount()-2];
						int k=0;
						for (int i = 2; i < table.getChildCount(); i++) 
						{
							View parentRow = table.getChildAt(i);
							if(parentRow instanceof TableRow)
							{
								TextView button = (TextView ) ((ViewGroup) parentRow).getChildAt(0);
					            if(button instanceof TextView)
					            {
					            	text[k] = button.getText().toString();
					            	k++;
					            }
								
							}
						}
						boolean playerNoExists = false;
						for (int p=0; p<k; p++)
						{
							if (playerNumber.toString().equalsIgnoreCase(text[p])==true)
							{
								Toast.makeText(getApplicationContext(),
										"The Player's number was introduced already for another player.",
										Toast.LENGTH_LONG).show();
								playerNoExists =true;
							}
						}
						if (playerNoExists == false)
						{
							if ((new Integer(playerNumber)<99)&& (new Integer(playerNumber)>0))
							{
								TableRow tr = new TableRow(table.getContext());
								if (playerNo % 2 != 0)
									tr.setBackgroundColor(Color.GRAY);
								tr.setId(100 + playerNo);
								tr.setLayoutParams(new LayoutParams(
										LayoutParams.WRAP_CONTENT,
										LayoutParams.WRAP_CONTENT));

								TextView player_number = new TextView(table.getContext());
								player_number.setId(500 + playerNo);
								player_number.setText(playerNumberEditText.getText()
										.toString());
								player_number.setPadding(5, 0, 15, 0);
								player_number.setTextColor(Color.WHITE);
								tr.addView(player_number);

								TextView player_position = new TextView(table.getContext());
								player_position.setId(300 + playerNo);
								player_position.setText(player_position_text);
								player_position.setPadding(2, 0, 5, 0);
								player_position.setTextColor(Color.WHITE);
								tr.addView(player_position);
								
								TextView player_name = new TextView(table.getContext());
								player_name.setId(400 + playerNo);
								player_name
										.setText(playerNameEditText.getText().toString());
								player_name.setPadding(5, 0, 15, 0);
								player_name.setTextColor(Color.WHITE);
								tr.addView(player_name);

								table.addView(tr, new TableLayout.LayoutParams(
										LayoutParams.WRAP_CONTENT,
										LayoutParams.WRAP_CONTENT));
								
								//add in myTeamData here
								ArrayList<String> helper = new ArrayList<String>();
								helper.add(playerNameEditText.getText().toString());
								helper.add(playerNumberEditText.getText()
										.toString());
								helper.add(player_position_text);
								myTeamData.add(helper);
								playerNo++;
							}
							else
							{
								Toast.makeText(getApplicationContext(),
										"The Player's number should be between 1 and 99.",
										Toast.LENGTH_LONG).show();
							}
						}
						
					}
					else
					{
						TableRow tr = new TableRow(table.getContext());
						if (playerNo % 2 != 0)
							tr.setBackgroundColor(Color.GRAY);
						tr.setId(100 + playerNo);
						tr.setLayoutParams(new LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));
	
						TextView player_number = new TextView(table.getContext());
						player_number.setId(500 + playerNo);
						player_number.setText(playerNumberEditText.getText()
								.toString());
						player_number.setPadding(5, 0, 15, 0);
						player_number.setTextColor(Color.WHITE);
						tr.addView(player_number);
	
						TextView player_position = new TextView(table.getContext());
						player_position.setId(300 + playerNo);
						player_position.setText(player_position_text);
						player_position.setPadding(2, 0, 5, 0);
						player_position.setTextColor(Color.WHITE);
						tr.addView(player_position);
						
						TextView player_name = new TextView(table.getContext());
						player_name.setId(400 + playerNo);
						player_name
								.setText(playerNameEditText.getText().toString());
						player_name.setPadding(5, 0, 15, 0);
						player_name.setTextColor(Color.WHITE);
						tr.addView(player_name);
	
						table.addView(tr, new TableLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));
						
						//add in myTeamData here
						ArrayList<String> helper = new ArrayList<String>();
						helper.add(playerNameEditText.getText().toString());
						helper.add(playerNumberEditText.getText()
								.toString());
						helper.add(player_position_text);
						myTeamData.add(helper);

						playerNo++;
					}
				}
			}
		});

		matchDateEditText = (EditText) findViewById(R.id.EditText02);
		matchDate = (Button) findViewById(R.id.Button01);

		matchDate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog(DATE_DIALOG_ID);
			}
		});

		// get the current date and time
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

		// display the current date
		displayDate();

	}

	// the callback received when the user
	// touches the SET option on the date dialog
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			displayDate();

			// INSERT or UPDATE the Match Date in HomeTeam Table here
		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		return new DatePickerDialog(this, mDateSetListener, mYear, mMonth, mDay);
	}

	// updates the date in the EditText
	private void displayDate() {
		matchDateEditText.setText(new StringBuilder()
				// Month is 0 based so add 1
				.append(mMonth + 1).append("/").append(mDay).append("/")
				.append(mYear).append(" "));
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}
}
