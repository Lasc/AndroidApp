package com.lasc.mcmullen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class HomeTeamDBHelper extends SQLiteOpenHelper 
{
	private static final String DB_NAME="stats.db";
	private static final int DB_VERSION=9; 
	
	private static final String TABLE_HOME_TEAM_EVENTS = "HomeTeamEvents";
	
	private static final String COLUMN_MATCH_ID = "MatchId";
	private static final String COLUMN_MINUTE = "Minute";
	private static final String COLUMN_EVENT_NAME = "EventName"; //name of the player
	private static final String COLUMN_PLAYER_NAME = "PlayerName";
	
	private static final String  TABLE_HOME_TEAM_PASS = "HomeTeamPasses";
	private static final String PASSES_PRIMARY_KEY = "ID INTEGER PRIMARY KEY AUTOINCREMENT";
	private static final String COLUMN_PASS1 = "ColumnPass1";
	private static final String COLUMN_PASS2 = "ColumnPass2";
	private static final String COLUMN_PASS3 = "ColumnPass3";
	private static final String COLUMN_PASS4 = "ColumnPass4";
	private static final String COLUMN_PASS5 = "ColumnPass5";
	private static final String COLUMN_PASS6 = "ColumnPass6";
	private static final String COLUMN_PASS7 = "ColumnPass7";
	private static final String COLUMN_PASS8 = "ColumnPass8";
	private static final String COLUMN_PASS9 = "ColumnPass9";
	private static final String COLUMN_PASS10 = "ColumnPass10";
	private static final String COLUMN_PASS11 = "ColumnPass11";
	private static final String COLUMN_PASS12 = "ColumnPass12";
	private static final String COLUMN_PASS13 = "ColumnPass13";
	private static final String COLUMN_PASS14 = "ColumnPass14";
	private static final String COLUMN_PASS15 = "ColumnPass15";
	

	private int RECORDED_PASSES = 0;
	private int LONGEST_PASS = 0;
	
	private static final String  TABLE_HOME_TEAM = "HomeTeamTable";
	private static final String PLAYER_POSITION = "PlayerPosition";
	private static final String PLAYER_NAME = "PlayerName";
	private static final String PLAYER_NUMBER = "PlayerNumber";
	private static final String MATCH_ID = "MathcId";
	
	private static final String TABLE_MATCH = "MatchTable";
	private static final String MATCHES_PRIMARY_KEY = "ID INTEGER PRIMARY KEY AUTOINCREMENT";
	private static final String HOME_TEAM = "HomeTeam";
	private static final String AWAY_TEAM = "AwayTeam";
	private static final String STATUS = "Status";
	private static final String LOCATION = "Location";
	private static final String MINUTE = "Minute";
	private static final String DATE = "Date";
	
	private static final String TABLE_OTHER_TEAM = "OtherTeam";
	
	private static final String HOME_TEAM_EVENTS_TABLE_CREATE = 
		"CREATE TABLE " + TABLE_HOME_TEAM_EVENTS 
		+ "(" + COLUMN_MINUTE + " TEXT, " 
		+ COLUMN_MATCH_ID + " TEXT, " 
		+ COLUMN_EVENT_NAME + " TEXT, " 
		+ COLUMN_PLAYER_NAME + " TEXT"
		+ ");";

	private static final String HOME_TEAM_PASS_REC_CREATE = 
			"CREATE TABLE " + TABLE_HOME_TEAM_PASS
			+ "(" + PASSES_PRIMARY_KEY + ", "
			+ COLUMN_MATCH_ID + " TEXT, " 
			+ COLUMN_PASS1 + " TEXT, " 
			+ COLUMN_PASS2 + " TEXT, "
			+ COLUMN_PASS3 + " TEXT, "
			+ COLUMN_PASS4 + " TEXT, "
			+ COLUMN_PASS5 + " TEXT, "
			+ COLUMN_PASS6 + " TEXT, "
			+ COLUMN_PASS7 + " TEXT, "
			+ COLUMN_PASS8 + " TEXT, "
			+ COLUMN_PASS9 + " TEXT, "
			+ COLUMN_PASS10 + " TEXT, "
			+ COLUMN_PASS11 + " TEXT, "
			+ COLUMN_PASS12 + " TEXT, "
			+ COLUMN_PASS13 + " TEXT, "
			+ COLUMN_PASS14 + " TEXT, "
			+ COLUMN_PASS15  + " TEXT"
			+ ");";
		
	private static final String HOME_TEAM_TABLE_CREATE = 
			"CREATE TABLE " + TABLE_HOME_TEAM
			+ "(" + MATCH_ID + " INTEGER, " 
			+ PLAYER_POSITION + " TEXT, " 
			+ PLAYER_NAME + " TEXT, "
			+ PLAYER_NUMBER + " INTEGER"
			+ ");";
	
	private static final String OTHER_TEAM_TABLE_CREATE = 
			"CREATE TABLE " + TABLE_OTHER_TEAM
			+ "(" + MATCH_ID + " INTEGER, " 
			+ PLAYER_POSITION + " TEXT, " 
			+ PLAYER_NAME + " TEXT, "
			+ PLAYER_NUMBER + " INTEGER"
			+ ");";
	
	private static final String MATCH_TABLE_CREATE = 
			"CREATE TABLE " + TABLE_MATCH
			+ "(" + MATCHES_PRIMARY_KEY + ", "
			+ HOME_TEAM + " TEXT, " 
			+ AWAY_TEAM + " TEXT, "
			+ STATUS + " TEXT, "
			+ LOCATION  + " TEXT, "
			+ MINUTE + " TEXT, "
			+ DATE  + " TEXT"
			+ ");";
	
	public HomeTeamDBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(HOME_TEAM_EVENTS_TABLE_CREATE);
		db.execSQL(HOME_TEAM_PASS_REC_CREATE);
		db.execSQL(HOME_TEAM_TABLE_CREATE);
		db.execSQL(MATCH_TABLE_CREATE);
		db.execSQL(OTHER_TEAM_TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_HOME_TEAM_EVENTS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_HOME_TEAM_PASS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_HOME_TEAM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MATCH);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_OTHER_TEAM);
		onCreate(db);
	}
	
	public ArrayList<ArrayList<String>> readPasses(SQLiteDatabase db)
	{
		int COLUMNs_NUMBER = 7;
		String [] columns = {"ID",COLUMN_PASS1,COLUMN_PASS2, COLUMN_PASS3, 
				COLUMN_PASS4, COLUMN_PASS5, COLUMN_PASS6, COLUMN_PASS7,
				COLUMN_PASS8, COLUMN_PASS9, COLUMN_PASS10, COLUMN_PASS11,
				COLUMN_PASS12, COLUMN_PASS13, COLUMN_PASS14, COLUMN_PASS15};
		
		ArrayList<ArrayList<String>>  passStatsArray;  
		Cursor cr = db.query(TABLE_HOME_TEAM_PASS, columns, null, null, null, null, null);
		int i = 0 ;
		if (cr!=null)
		{
			passStatsArray =  new ArrayList<>();

			if (cr.moveToFirst())
				do 
				{
					ArrayList<String> passArrayHelper = new ArrayList<>();
					for (int p = 0; p<COLUMNs_NUMBER; p++)
						if (!cr.isNull(p))
							passArrayHelper.add(cr.getString(p));
							
					passStatsArray.add(passArrayHelper);
					i++;
					
				} while (cr.moveToNext());
			
			RECORDED_PASSES = passStatsArray.size();
			
			for (int k=0;k<RECORDED_PASSES; k++)
				if (LONGEST_PASS < passStatsArray.get(k).size())
					LONGEST_PASS =  passStatsArray.get(k).size();
		}
		else
			return null;
		
		return passStatsArray;
	}
	
	public Map<String, ArrayList<ArrayList<String>>> readPassStats(SQLiteDatabase db)
	{
		int passEndOut = 0;
		int passEndCorner = 0;
		int passEndPossessionLost = 0;
		int passEndGoal = 0;
		int passEndOffSide= 0;
		
		int COLUMNs_NUMBER = 15;
		String [] columns = {"ID",COLUMN_PASS1,COLUMN_PASS2, COLUMN_PASS3, 
				COLUMN_PASS4, COLUMN_PASS5, COLUMN_PASS6, COLUMN_PASS7,
				COLUMN_PASS8, COLUMN_PASS9, COLUMN_PASS10, COLUMN_PASS11,
				COLUMN_PASS12, COLUMN_PASS13, COLUMN_PASS14, COLUMN_PASS15};
		
		ArrayList<ArrayList<String>>  passOuts, passCorners,
								passPossessionLoss, passGoals, passOffside;  
		Cursor cr = db.query(TABLE_HOME_TEAM_PASS, columns, null, null, null, null, null);
		
		HashMap<String, ArrayList<ArrayList<String>>> passStatsMap = new HashMap<>();
		
		if (cr!=null)
		{
			passOuts = new ArrayList<>();
			passCorners = new ArrayList<>();
			passPossessionLoss = new ArrayList<>();
			passGoals = new ArrayList<>();
			passOffside = new ArrayList<>();
			
			if (cr.moveToFirst())
				do 
				{
					// store each pass sequence in passArrayHelper
					ArrayList<String> passArrayHelper = new ArrayList<>();
					for (int p = 0; p<COLUMNs_NUMBER; p++)
						if (!cr.isNull(p))
							passArrayHelper.add(cr.getString(p));
					
					// if the pass ended in Out, Corner,
					// Lost of Possesion, Goal or Offside 
					// count and add to the stats array
					if (passArrayHelper.get(passArrayHelper.size()-1).equalsIgnoreCase("Out"))
					{
						passOuts.add(passArrayHelper);
					}
					
					if (passArrayHelper.get(passArrayHelper.size()-1).equalsIgnoreCase("Corner"))
					{
						passCorners.add(passArrayHelper);
					}
					
					if (passArrayHelper.get(passArrayHelper.size()-1).equalsIgnoreCase("Lost Possession"))
					{
						passPossessionLoss.add(passArrayHelper);
					}
					
					if (passArrayHelper.get(passArrayHelper.size()-1).equalsIgnoreCase("Goal"))
					{
						passGoals.add(passArrayHelper);
					}
					
					if (passArrayHelper.get(passArrayHelper.size()-1).equalsIgnoreCase("Offside"))
					{
						passOffside.add(passArrayHelper);
					}

				} while (cr.moveToNext());
			passStatsMap.put("Out", passOuts);
			passStatsMap.put("Corner", passCorners);
			passStatsMap.put("Lost Possession", passPossessionLoss);
			passStatsMap.put("Goal", passGoals);
			passStatsMap.put("Offside", passOffside);

		}
		else
			return null;
		
		return passStatsMap;
	}
	
	public ArrayList<ArrayList<String>> readGeneralEvents(SQLiteDatabase db)
	{
		int COLUMNs_NUMBER = 3;
		String [] columns = {COLUMN_MINUTE,COLUMN_EVENT_NAME,COLUMN_PLAYER_NAME};
		ArrayList<ArrayList<String>> passStatsArray;  
		Cursor cr = db.query(TABLE_HOME_TEAM_EVENTS, columns, null, null, null, null, null);
		int i = 0 ;
		if (cr!=null)
		{
			passStatsArray =  new ArrayList<>();

			if (cr.moveToFirst())
				do 
				{
					ArrayList<String> passArrayHelper = new ArrayList<>();
					for (int p = 0; p<COLUMNs_NUMBER; p++)
						if (!cr.isNull(p))
								passArrayHelper.add(cr.getString(p));
							
					passStatsArray.add(passArrayHelper);
					i++;
					
				} while (cr.moveToNext());
			
			RECORDED_PASSES = passStatsArray.size();
			
			for (int k=0;k<RECORDED_PASSES; k++)
				if (LONGEST_PASS < passStatsArray.get(k).size())
					LONGEST_PASS =  passStatsArray.get(k).size();
		}
		else
			return null;
		
		return passStatsArray;
	}

}
