package com.lasc.mcmullen.tests;

import com.lasc.mcmullen.AddAwayTeamActivity;
import com.lasc.mcmullen.R;

import android.test.ActivityInstrumentationTestCase2;
import android.test.ViewAsserts;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;


public class AddAwayTeamActivityTest extends ActivityInstrumentationTestCase2<AddAwayTeamActivity> {

	private AddAwayTeamActivity mAddAwayTeamActivity;
	private Button mAddOtherTeamButton, mAddPlayerButton, mAddMatchLocation;
	final String resultText = "Left-Midfield"; 
	private EditText mTeamNameEditBox, mPlayerNameEdit, mPlayerNoEdit, mLocationEdit;
	private Spinner mSpinner;
    public static final int INITIAL_POSITION = 0;
    public static final int TEST_POSITION = 5;
    private String mSelection;
    private int mPos;
	
	@Override
    protected void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(true);
        mAddAwayTeamActivity = getActivity();
        mAddOtherTeamButton = (Button) mAddAwayTeamActivity.findViewById(R.id.button2);
        mTeamNameEditBox = (EditText) mAddAwayTeamActivity.findViewById(R.id.EditText01);
        mAddPlayerButton = (Button)  mAddAwayTeamActivity.findViewById(R.id.foulButton);
        mSpinner = (Spinner) mAddAwayTeamActivity.findViewById(R.id.player_position);
        mPlayerNameEdit= (EditText) mAddAwayTeamActivity.findViewById(R.id.textView2);
        mAddMatchLocation = (Button)  mAddAwayTeamActivity.findViewById(R.id.button3);
        mPlayerNoEdit = (EditText) mAddAwayTeamActivity.findViewById(R.id.textView3);
        mLocationEdit = (EditText) mAddAwayTeamActivity.findViewById(R.id.EditText02);
    }
	
	public AddAwayTeamActivityTest() {
		super(AddAwayTeamActivity.class);
	}
	
    @MediumTest
    public void testAddOtherTeamButton() {
        final View decorView = mAddAwayTeamActivity.getWindow().getDecorView();
        
        ViewAsserts.assertOnScreen(decorView, mAddOtherTeamButton);

        final ViewGroup.LayoutParams layoutParams =
        		mAddOtherTeamButton.getLayoutParams();
        assertNotNull(layoutParams);
        assertEquals(layoutParams.width, WindowManager.LayoutParams.WRAP_CONTENT);
        assertEquals(layoutParams.height, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    @MediumTest
    public void testOtherTeamNameEditText() {
        final View decorView = mAddAwayTeamActivity.getWindow().getDecorView();

        ViewAsserts.assertOnScreen(decorView, mTeamNameEditBox);

        final ViewGroup.LayoutParams layoutParams =
        		mTeamNameEditBox.getLayoutParams();
        assertNotNull(layoutParams);
        assertEquals(layoutParams.width, WindowManager.LayoutParams.WRAP_CONTENT);
        assertEquals(layoutParams.height, WindowManager.LayoutParams.WRAP_CONTENT);
        assertEquals("Type Team Name" , mTeamNameEditBox.getHint());
    }
    
    @MediumTest
    public void testAddPlayerButton() {
        final View decorView = mAddAwayTeamActivity.getWindow().getDecorView();

        ViewAsserts.assertOnScreen(decorView, mAddPlayerButton);

        final ViewGroup.LayoutParams layoutParams =
        		mAddPlayerButton.getLayoutParams();
        assertNotNull(layoutParams);
        assertEquals(layoutParams.width, WindowManager.LayoutParams.WRAP_CONTENT);
        assertEquals(layoutParams.height, WindowManager.LayoutParams.WRAP_CONTENT);
    }
    
    @MediumTest
    public void testPLayerDropDownBoxBehaviour() {
    	
    	mAddAwayTeamActivity.runOnUiThread(new Runnable() 
    	{
    		public void run() 
    		{
    			mSpinner.requestFocus();
    			mSpinner.setSelection(TEST_POSITION);
    	    	mPos = mSpinner.getSelectedItemPosition();
    	    	mSelection = (String) mSpinner.getItemAtPosition(mPos);
    	    	assertEquals(resultText, mSelection);
    		} 
    	}); 
    }
    
    @MediumTest
    public void testPlayerNameEditText() {
        final View decorView = mAddAwayTeamActivity.getWindow().getDecorView();

        ViewAsserts.assertOnScreen(decorView, mPlayerNameEdit);

        final ViewGroup.LayoutParams layoutParams =
        		mPlayerNameEdit.getLayoutParams();
        assertNotNull(layoutParams);
        assertEquals(layoutParams.width, WindowManager.LayoutParams.WRAP_CONTENT);
        assertEquals(layoutParams.height, WindowManager.LayoutParams.WRAP_CONTENT);
        assertEquals("Type Player Name" , mPlayerNameEdit.getHint());
    }
    
    @MediumTest
    public void testMatchLocationButton() {
        final View decorView = mAddAwayTeamActivity.getWindow().getDecorView();

        ViewAsserts.assertOnScreen(decorView, mAddMatchLocation);

        final ViewGroup.LayoutParams layoutParams =
        		mAddMatchLocation.getLayoutParams();
        assertNotNull(layoutParams);
        assertEquals(layoutParams.width, WindowManager.LayoutParams.WRAP_CONTENT);
        assertEquals(layoutParams.height, WindowManager.LayoutParams.WRAP_CONTENT);
    }
    
    @MediumTest
    public void testPlayerNumberEditText() {
        final View decorView = mAddAwayTeamActivity.getWindow().getDecorView();

        ViewAsserts.assertOnScreen(decorView, mPlayerNoEdit);

        final ViewGroup.LayoutParams layoutParams =
        		mPlayerNoEdit.getLayoutParams();
        assertNotNull(layoutParams);
        assertEquals(layoutParams.width, WindowManager.LayoutParams.WRAP_CONTENT);
        assertEquals(layoutParams.height, WindowManager.LayoutParams.WRAP_CONTENT);
        assertEquals("Type Player Number" , mPlayerNoEdit.getHint());
    }
    
    @MediumTest
    public void testMatchLocationEditText() {
        final View decorView = mAddAwayTeamActivity.getWindow().getDecorView();

        ViewAsserts.assertOnScreen(decorView, mLocationEdit);

        final ViewGroup.LayoutParams layoutParams =
        		mLocationEdit.getLayoutParams();
        assertNotNull(layoutParams);
        assertEquals(layoutParams.width, WindowManager.LayoutParams.WRAP_CONTENT);
        assertEquals(layoutParams.height, WindowManager.LayoutParams.WRAP_CONTENT);
        assertEquals("Type Location" , mLocationEdit.getHint());
    }
}
