package com.lasc.mcmullen.tests;

import com.lasc.mcmullen.MainActivity;
import com.lasc.mcmullen.R;

import android.test.ActivityInstrumentationTestCase2;
import android.test.ViewAsserts;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
    private MainActivity mMainActivity;
    private Button mAddNewTeamButton, mLiveGameRecordingButton, mLeagueTableButton, mFixturesButton;

    public MainActivityTest() {
        super(MainActivity.class);
    }

	@Override
    protected void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(true);
        mMainActivity = getActivity();
//        mAddNewTeamButton = (Button) mMainActivity.findViewById(R.id.button4);
        mLiveGameRecordingButton = (Button) mMainActivity.findViewById(R.id.button1);
        mLeagueTableButton = (Button) mMainActivity.findViewById(R.id.button2);
        mFixturesButton =  (Button) mMainActivity.findViewById(R.id.button3);
    }
    
//    @MediumTest
//    public void testAddNewTeamButton() {
//        final View decorView = mMainActivity.getWindow().getDecorView();
//
//        ViewAsserts.assertOnScreen(decorView, mAddNewTeamButton);
//
//        final ViewGroup.LayoutParams layoutParams =
//        		mAddNewTeamButton.getLayoutParams();
//        assertNotNull(layoutParams);
//        assertEquals(layoutParams.width, WindowManager.LayoutParams.WRAP_CONTENT);
//        assertEquals(layoutParams.height, WindowManager.LayoutParams.WRAP_CONTENT);
//    }
    
    @MediumTest
    public void testLiveGameRecordingButton() {
        final View decorView = mMainActivity.getWindow().getDecorView();

        ViewAsserts.assertOnScreen(decorView, mLiveGameRecordingButton);

        final ViewGroup.LayoutParams layoutParams =
        		mLiveGameRecordingButton.getLayoutParams();
        assertNotNull(layoutParams);
        assertEquals(layoutParams.width, WindowManager.LayoutParams.WRAP_CONTENT);
        assertEquals(layoutParams.height, WindowManager.LayoutParams.WRAP_CONTENT);
    }
    
    @MediumTest
    public void testLeagueTableButton() {
        final View decorView = mMainActivity.getWindow().getDecorView();

        ViewAsserts.assertOnScreen(decorView, mLeagueTableButton);

        final ViewGroup.LayoutParams layoutParams =
        		mLeagueTableButton.getLayoutParams();
        assertNotNull(layoutParams);
        assertEquals(layoutParams.width, WindowManager.LayoutParams.WRAP_CONTENT);
        assertEquals(layoutParams.height, WindowManager.LayoutParams.WRAP_CONTENT);
    }
    
    @MediumTest
    public void testFixturesButton() {
        final View decorView = mMainActivity.getWindow().getDecorView();

        ViewAsserts.assertOnScreen(decorView, mFixturesButton);

        final ViewGroup.LayoutParams layoutParams =
        		mFixturesButton.getLayoutParams();
        assertNotNull(layoutParams);
        assertEquals(layoutParams.width, WindowManager.LayoutParams.WRAP_CONTENT);
        assertEquals(layoutParams.height, WindowManager.LayoutParams.WRAP_CONTENT);
    }
}
