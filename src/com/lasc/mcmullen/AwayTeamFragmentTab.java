package com.lasc.mcmullen;

import android.app.Fragment;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class AwayTeamFragmentTab extends Fragment {
	SQLiteOpenHelper dbhelper;
	SQLiteDatabase database;
	
	ToggleButton goal;
	ImageButton player1, player2, player3, player4, player5, player6, player7,
	player8, player9, player10, player11;
	
	TextView player1Text, player1NoText, player2Text, player2NoText,
	player3Text, player3NoText, player4Text, player4NoText,
	player5Text, player5NoText, player6Text, player6NoText,
	player7Text, player7NoText, player8Text, player8NoText,
	player9Text, player9NoText, player10Text, player10NoText,
	player11Text, player11NoText;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.away_tab_layout, container, false);
		goal = (ToggleButton) rootView.findViewById(R.id.toggleButton2);
		dbhelper = new HomeTeamDBHelper(getActivity());

		
		player1Text = (TextView) rootView
				.findViewById(R.id.TextView1);

		player1NoText = (TextView) rootView
				.findViewById(R.id.TextView09);
		
		player1NoText.setText(ApplicationContextProvider.oplayerNO.get(0));
		player1Text.setText(ApplicationContextProvider.oplayerSN.get(0));
		
		player1 = (ImageButton) rootView.findViewById(R.id.imageButton1);
		player1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (goal.isChecked()) {
					// record just the goal as a general event
					ContentValues content = new ContentValues();
					content.put("Minute",
							(String) StatsFragmentTab.getTime());
					content.put("EventName", "Goal");
					content.put(
							"PlayerName",
							ApplicationContextProvider.oplayerSN.get(0)
							+ " (No."
							+ ApplicationContextProvider.oplayerNO
									.get(0) +")");
					database = dbhelper.getWritableDatabase();
					database.insert("HomeTeamEvents", null, content);
					database.close();
					
					Toast.makeText(
							getActivity(),
							"GOAL - "
									+ ApplicationContextProvider.oplayerSN.get(0)
									+ " (No."
									+ ApplicationContextProvider.oplayerNO
											.get(0) +")", Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
		
		
		// #############    PLAYER 2   ##################
		player2Text = (TextView) rootView
				.findViewById(R.id.TextView2);

		player2NoText = (TextView) rootView
				.findViewById(R.id.TextView07);
	
		player2NoText.setText(ApplicationContextProvider.oplayerNO
				.get(1));
		player2Text.setText(ApplicationContextProvider.oplayerSN.get(1));

		player2 = (ImageButton) rootView.findViewById(R.id.ImageButton01);
		player2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (goal.isChecked()) {
					// record just the goal as a general event
					ContentValues content = new ContentValues();
					content.put("Minute",
							(String) StatsFragmentTab.getTime());
					content.put("EventName", "Goal received");
					content.put(
							"PlayerName",
							ApplicationContextProvider.oplayerSN.get(1)
							+ " (No."
							+ ApplicationContextProvider.oplayerNO
									.get(1) +")");
					database = dbhelper.getWritableDatabase();
					database.insert("HomeTeamEvents", null, content);
					database.close();

					
					Toast.makeText(
							getActivity(),
							"GOAL - "
									+ ApplicationContextProvider.oplayerSN.get(1)
									+ " (No."
									+ ApplicationContextProvider.oplayerNO
											.get(1) +")", Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
		
		// ############   PLAYER 3     #######################
		player3Text = (TextView) rootView
				.findViewById(R.id.TextView3);

		player3NoText = (TextView) rootView
				.findViewById(R.id.TextView08);
		
		player3NoText.setText(ApplicationContextProvider.oplayerNO
				.get(2));
		player3Text.setText(ApplicationContextProvider.oplayerSN.get(2));

		player3 = (ImageButton) rootView.findViewById(R.id.ImageButton02);
		player3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (goal.isChecked()) {
					// record just the goal as a general event
					ContentValues content = new ContentValues();
					content.put("Minute",
							(String) StatsFragmentTab.getTime());
					content.put("EventName", "Goal received");
					content.put(
							"PlayerName",
							ApplicationContextProvider.oplayerSN.get(2)
							+ " (No."
							+ ApplicationContextProvider.oplayerNO
									.get(2) +")");
					database = dbhelper.getWritableDatabase();
					database.insert("HomeTeamEvents", null, content);
					database.close();

					Toast.makeText(
							getActivity(),
							"GOAL - "
									+ ApplicationContextProvider.oplayerSN.get(2)
									+ " (No."
									+ ApplicationContextProvider.oplayerNO
											.get(2) +")", Toast.LENGTH_SHORT)
							.show();
				}
	
			}
		});
		
		
		// ############   PLAYER 4     #######################
		player4Text = (TextView) rootView
				.findViewById(R.id.TextView4);

		player4NoText = (TextView) rootView
				.findViewById(R.id.TextView10);
		
		player4NoText.setText(ApplicationContextProvider.oplayerNO.get(3));
		player4Text.setText(ApplicationContextProvider.oplayerSN.get(3));

		player4 = (ImageButton) rootView.findViewById(R.id.ImageButton03);
		player4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
		
				if (goal.isChecked()) {
					// record just the goal as a general event
					ContentValues content = new ContentValues();
					content.put("Minute",
							(String) StatsFragmentTab.getTime());
					content.put("EventName", "Goal received");
					content.put(
							"PlayerName",
							ApplicationContextProvider.oplayerSN.get(3)
							+ " (No."
							+ ApplicationContextProvider.oplayerNO
									.get(3) +")");
					database = dbhelper.getWritableDatabase();
					database.insert("HomeTeamEvents", null, content);
					database.close();
					
					Toast.makeText(
							getActivity(),
							"GOAL - "
									+ ApplicationContextProvider.oplayerSN.get(3)
									+ " (No."
									+ ApplicationContextProvider.oplayerNO.get(3) +")",
									Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
		
		// ############   PLAYER 5     #######################
		player5Text = (TextView) rootView
				.findViewById(R.id.TextView5);

		player5NoText = (TextView) rootView
				.findViewById(R.id.TextView11);

		player5NoText.setText(ApplicationContextProvider.oplayerNO.get(4));
		player5Text.setText(ApplicationContextProvider.oplayerSN.get(4));

		player5 = (ImageButton) rootView.findViewById(R.id.ImageButton04);
		player5.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (goal.isChecked()) {
					// record just the goal as a general event
					ContentValues content = new ContentValues();
					content.put("Minute",
							(String) StatsFragmentTab.getTime());
					content.put("EventName", "Goal");
					content.put(
							"PlayerName",
							ApplicationContextProvider.oplayerSN.get(4)
							+ " (No."
							+ ApplicationContextProvider.oplayerNO
									.get(4) +")");
					database = dbhelper.getWritableDatabase();
					database.insert("HomeTeamEvents", null, content);
					database.close();

					Toast.makeText(
							getActivity(),
							"GOAL - "
									+ ApplicationContextProvider.oplayerSN.get(4)
									+ " (No."
									+ ApplicationContextProvider.oplayerNO
											.get(4) +")", Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
		
		// ############   PLAYER 6     #######################
		player6Text = (TextView) rootView
				.findViewById(R.id.TextView01);

		player6NoText = (TextView) rootView
				.findViewById(R.id.textView6);
		
		
		player6NoText.setText(ApplicationContextProvider.playerNO.get(5));
		player6Text.setText(ApplicationContextProvider.playerSN.get(5));

		player6 = (ImageButton) rootView.findViewById(R.id.ImageButton05);
		player6.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (goal.isChecked()) {
					// record just the goal as a general event
					ContentValues content = new ContentValues();
					content.put("Minute",
							(String) StatsFragmentTab.getTime());
					content.put("EventName", "Goal received");
					content.put(
							"PlayerName",
							ApplicationContextProvider.oplayerSN.get(5)
							+ " (No."
							+ ApplicationContextProvider.oplayerNO
									.get(5) +")");
					database = dbhelper.getWritableDatabase();
					database.insert("HomeTeamEvents", null, content);
					database.close();

					Toast.makeText(
							getActivity(),
							"GOAL - "
									+ ApplicationContextProvider.oplayerSN.get(5)
									+ " (No."
									+ ApplicationContextProvider.oplayerNO
											.get(5) +")", Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
		
		// ############   PLAYER 7     #######################
		player7Text = (TextView) rootView
				.findViewById(R.id.TextView02);

		player7NoText = (TextView) rootView
				.findViewById(R.id.TextView12);

		player7NoText.setText(ApplicationContextProvider.oplayerNO.get(6));
		player7Text.setText(ApplicationContextProvider.oplayerSN.get(6));

		player7 = (ImageButton) rootView.findViewById(R.id.ImageButton06);
		player7.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (goal.isChecked()) {
					// record just the goal as a general event
					ContentValues content = new ContentValues();
					content.put("Minute",
							(String) StatsFragmentTab.getTime());
					content.put("EventName", "Goal received");
					content.put(
							"PlayerName",
							ApplicationContextProvider.oplayerSN.get(6)
							+ " (No."
							+ ApplicationContextProvider.oplayerNO
									.get(6) +")");
					database = dbhelper.getWritableDatabase();
					database.insert("HomeTeamEvents", null, content);
					database.close();

					
					Toast.makeText(
							getActivity(),
							"GOAL - "
									+ ApplicationContextProvider.oplayerSN.get(6)
									+ " (No."
									+ ApplicationContextProvider.oplayerNO
											.get(6) +")", Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
		
		
		// ############   PLAYER 8     #######################
		player8Text = (TextView) rootView
				.findViewById(R.id.TextView03);

		player8NoText = (TextView) rootView
				.findViewById(R.id.TextView13);

		player8NoText.setText(ApplicationContextProvider.oplayerNO.get(7));
		player8Text.setText(ApplicationContextProvider.oplayerSN.get(7));

		player8 = (ImageButton) rootView.findViewById(R.id.ImageButton07);
		player8.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (goal.isChecked()) {
					// record just the goal as a general event
					ContentValues content = new ContentValues();
					content.put("Minute",
							(String) StatsFragmentTab.getTime());
					content.put("EventName", "Goal received");
					content.put(
							"PlayerName",
							ApplicationContextProvider.oplayerSN.get(7)
							+ " (No."
							+ ApplicationContextProvider.oplayerNO
									.get(7) +")");
					database = dbhelper.getWritableDatabase();
					database.insert("HomeTeamEvents", null, content);
					database.close();
					
					Toast.makeText(
							getActivity(),
							"GOAL - "
									+ ApplicationContextProvider.oplayerSN.get(7)
									+ " (No."
									+ ApplicationContextProvider.oplayerNO
											.get(7) +")", Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
		
		// ############   PLAYER 9     #######################
		player9Text = (TextView) rootView
				.findViewById(R.id.TextView04);

		player9NoText = (TextView) rootView
				.findViewById(R.id.TextView14);

		player9NoText.setText(ApplicationContextProvider.oplayerNO.get(8));
		player9Text.setText(ApplicationContextProvider.oplayerSN.get(8));

		player9 = (ImageButton) rootView.findViewById(R.id.ImageButton08);
		player9.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (goal.isChecked()) {
					// record just the goal as a general event
					ContentValues content = new ContentValues();
					content.put("Minute",
							(String) StatsFragmentTab.getTime());
					content.put("EventName", "Goal received");
					content.put(
							"PlayerName",
							ApplicationContextProvider.oplayerSN.get(8)
							+ " (No."
							+ ApplicationContextProvider.oplayerNO
									.get(8) +")");
					database = dbhelper.getWritableDatabase();
					database.insert("HomeTeamEvents", null, content);
					database.close();

					Toast.makeText(
							getActivity(),
							"GOAL - "
									+ ApplicationContextProvider.oplayerSN.get(8)
									+ " (No."
									+ ApplicationContextProvider.oplayerNO
											.get(8) +")", Toast.LENGTH_SHORT)
							.show();
				}

			}
		});
		
		

		// ############   PLAYER 10     #######################
		player10Text = (TextView) rootView
				.findViewById(R.id.TextView05);

		player10NoText = (TextView) rootView
				.findViewById(R.id.TextView15);
		
		player10NoText.setText(ApplicationContextProvider.oplayerNO.get(9));
		player10Text.setText(ApplicationContextProvider.oplayerSN.get(9));

		player10 = (ImageButton) rootView.findViewById(R.id.ImageButton09);
		player10.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// GOAL
				if (goal.isChecked()) {
					// record just the goal as a general event
					ContentValues content = new ContentValues();
					content.put("Minute",
							(String) StatsFragmentTab.getTime());
					content.put("EventName", "Goal received");
					content.put(
							"PlayerName",
							ApplicationContextProvider.oplayerSN.get(9)
							+ " (No."
							+ ApplicationContextProvider.oplayerNO
									.get(9) +")");
					database = dbhelper.getWritableDatabase();
					database.insert("HomeTeamEvents", null, content);
					database.close();

					Toast.makeText(
							getActivity(),
							"GOAL - "
									+ ApplicationContextProvider.oplayerSN.get(9)
									+ " (No."
									+ ApplicationContextProvider.oplayerNO
											.get(9) +")", Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
		
		// ############   PLAYER 11     #######################
		player11Text = (TextView) rootView
				.findViewById(R.id.TextView06);

		player11NoText = (TextView) rootView
				.findViewById(R.id.TextView16);
		
		player11NoText.setText(ApplicationContextProvider.oplayerNO.get(10));
		player11Text.setText(ApplicationContextProvider.oplayerSN.get(10));

		player11 = (ImageButton) rootView.findViewById(R.id.ImageButton10);
		player11.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// GOAL
				if (goal.isChecked()) {
					// record just the goal as a general event
					ContentValues content = new ContentValues();
					content.put("Minute",
							(String) StatsFragmentTab.getTime());
					content.put("EventName", "Goal received");
					content.put(
							"PlayerName",
							ApplicationContextProvider.oplayerSN.get(10)
							+ " (No."
							+ ApplicationContextProvider.oplayerNO
									.get(10) +")");
					database = dbhelper.getWritableDatabase();
					database.insert("HomeTeamEvents", null, content);
					database.close();


					Toast.makeText(
							getActivity(),
							"GOAL - "
									+ ApplicationContextProvider.oplayerSN.get(10)
									+ " (No."
									+ ApplicationContextProvider.oplayerNO
											.get(10) +")", Toast.LENGTH_SHORT)
							.show();
				}

			}
		});
		return rootView;
	}
	
}
